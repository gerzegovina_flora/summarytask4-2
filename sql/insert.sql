insert into "DOCTOR_TYPE" (id ,"type") VALUES (0,'NEUROLOGIST');
insert into "DOCTOR_TYPE" (id ,"type") VALUES (1,'ALLERGIST');
insert into "DOCTOR_TYPE" (id ,"type") VALUES (2,'ANDROLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (3,'OBSTETRICIAN');
insert into "DOCTOR_TYPE" (id,"type") VALUES (4,'ENDOCRINOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (5,'ONCOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (6,'PROCTOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (7,'OTOLARYNGOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (8,'PEDIATRICIAN');
insert into "DOCTOR_TYPE" (id,"type") VALUES (9,'IMMUNOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (10,'CARDIOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (11,'GASTROENTEROLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (12,'TRAUMATOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (13,'UROLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (14,'OPTOMETRIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (15,'NARCOLOGY');
insert into "DOCTOR_TYPE" (id,"type") VALUES (16,'SURGEON');
insert into "DOCTOR_TYPE" (id,"type") VALUES (17,'RADIOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (18,'GYNECOLOGIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (19,'MAMMOLOG');
insert into "DOCTOR_TYPE" (id,"type") VALUES (20,'SPEECH_THERAPIST');
insert into "DOCTOR_TYPE" (id,"type") VALUES (21,'NUTRITIONIST');

INSERT INTO "USER" (login, name, role, password) VALUES ('administrator', 'Admin', 0, md5('1111'));

--INSERT DOCTORS--

INSERT INTO "USER" (login, name, role, password, image,last_name,middle_name) VALUES ('doctor1', 'Doctor1', 1, md5('doctor1'),DEFAULT , 'DOCTOR_L','DOCTOR_M');
INSERT INTO "DOCTOR" (id_user, id_type) VALUES (currval(pg_get_serial_sequence('"USER"','id')), 1);

INSERT INTO "USER" (login, name, role, password, image,last_name,middle_name) VALUES ('doctor2', 'Doctor2', 1, md5('doctor2'),DEFAULT , 'DOCTOR_L','DOCTOR_M');
INSERT INTO "DOCTOR" (id_user, id_type) VALUES (currval(pg_get_serial_sequence('"USER"','id')), 1);

INSERT INTO "USER" (login, name, role, password, image,last_name,middle_name) VALUES ('doctor3', 'Doctor3', 1, md5('doctor3'),DEFAULT , 'DOCTOR_L','DOCTOR_M');
INSERT INTO "DOCTOR" (id_user, id_type) VALUES (currval(pg_get_serial_sequence('"USER"','id')), 1);

INSERT INTO "USER" (login, name, role, password,image,last_name,middle_name) VALUES ('doctor4', 'Doctor4', 1, md5('doctor4'),DEFAULT , 'DOCTOR_L','DOCTOR_M');
INSERT INTO "DOCTOR" (id_user, id_type) VALUES (currval(pg_get_serial_sequence('"USER"','id')), 1);

INSERT INTO "USER" (login, name, role, password,image,last_name,middle_name) VALUES ('doctor5', 'Doctor5', 1, md5('doctor5'),DEFAULT , 'DOCTOR_L','DOCTOR_M');
INSERT INTO "DOCTOR" (id_user, id_type) VALUES (currval(pg_get_serial_sequence('"USER"','id')), 1);

--INSERT NURSES--

INSERT INTO "USER" (login, name, role, password,image,last_name,middle_name) VALUES ('nurse1', 'Nurse1', 2, md5('nurse1'),DEFAULT , 'NURSE_L','NURSE_M');
INSERT INTO "NURSE" (id_user) VALUES (currval(pg_get_serial_sequence('"USER"','id')));

INSERT INTO "USER" (login, name, role, password,image,last_name,middle_name) VALUES ('nurse2', 'Nurse2', 2, md5('nurse2'),DEFAULT , 'NURSE_L','NURSE_M');
INSERT INTO "NURSE" (id_user) VALUES (currval(pg_get_serial_sequence('"USER"','id')));

INSERT INTO "USER" (login, name, role, password,image,last_name,middle_name) VALUES ('nurse3', 'Nurse3', 2, md5('nurse2'),DEFAULT , 'NURSE_L','NURSE_M');
INSERT INTO "NURSE" (id_user) VALUES (currval(pg_get_serial_sequence('"USER"','id')));