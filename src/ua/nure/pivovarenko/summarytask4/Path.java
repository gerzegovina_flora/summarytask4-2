package ua.nure.pivovarenko.SummaryTask4;

/**
 * Path holder (jsp pages, controller commands).
 * 
 * @author D.Kolesnikov
 * 
 */
public final class Path {

	private  Path(){}

	//pages
	public static final String PAGE_PATIENCE_LIST = "/pages/patient-list.jsp";
	public static final String PAGE_MY_PATIENCE_LIST = "/pages/my-patient-list.jsp";
	public static final String PAGE_DOCTOR_LIST = "/pages/doctor-list.jsp";
	public static final String PAGE_ADD_NURSE = "/pages/add-nurse.jsp";
	public static final String PAGE_ADD_DOCTOR = "/pages/add-doctor.jsp";
	public static final String PAGE_ADD_PATIENT = "/pages/add-patient.jsp";
	public static final String PAGE_NURSE_LIST = "/pages/nurse-list.jsp";
	public static final String PAGE_PATIENCE_PROFILE = "/pages/profile-patient-view.jsp";
	public static final String PAGE_PROFILE = "/pages/index.jsp";
	public static final String PAGE_EDIT_NURSE = "/pages/edit-nurse.jsp";
	public static final String PAGE_EDIT_DOCTOR = "/pages/edit-doctor.jsp";
	public static final String PAGE_EDIT_PROFILE = "/pages/edit-profile.jsp";
	public static final String PAGE_EDIT_PATIENT = "/pages/edit-patient.jsp";
	public static final String PAGE_INDEX = "/index.jsp";

}