package ua.nure.pivovarenko.SummaryTask4.validate;

import ua.nure.pivovarenko.SummaryTask4.domain.form.AddPatientForm;
import ua.nure.pivovarenko.SummaryTask4.util.ValidationUtil;

import java.util.Objects;

public class AddPatientValidator extends Validator{

    private static final String NAME = "^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \\-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}";
    private static final String PHONE_NUMBER = "^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$";
    private static final String DATE = "20\\d{2}(-|\\/)((0[1-9])|(1[0-2]))(-|\\/)((0[1-9])|([1-2][0-9])|(3[0-1]))";
    private static final String DIGIT = "^[1-9][0-9]{0,6}";

    @Override
    public boolean isValid(Object o) {
        AddPatientForm patient = (AddPatientForm)o;
        ValidationUtil.validateValue(errors, patient.getFirstName(), NAME, "error.user.name.first");
        ValidationUtil.validateValue(errors, patient.getSecondName(),NAME, "error.user.name.second");
        ValidationUtil.validateValue(errors, patient.getMiddleName(),NAME, "error.user.name.middle");
        if(!Objects.equals(patient.getPhone(), "")){
            ValidationUtil.validateValue(errors, patient.getPhone(),PHONE_NUMBER, "error.user.phone");
        }
        ValidationUtil.validateValue(errors, patient.getIdDoctor(),DIGIT, "error.user.treatment.doctor");
        return errors.hasErrors();
    }
}
