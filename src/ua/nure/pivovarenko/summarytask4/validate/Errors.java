package ua.nure.pivovarenko.SummaryTask4.validate;

import java.util.HashMap;
import java.util.Map;

public final class Errors {

    public class Result{

        private boolean result;
        private String value;

        public Result() {
        }

        public Result(boolean result, String value) {
            this.result = result;
            this.value = value;
        }

        public boolean isResult() {
            return result;
        }

        public void setResult(boolean result) {
            this.result = result;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public void setError(String key, String value, boolean result){
        errors.put(key, new Result(result, value));
    }

    private final Map<String, Result> errors = new HashMap<>();

    public Map<String, Result> getErrors() {
        return errors;
    }

    public boolean hasErrors(){
        for(Result r : errors.values()){
            if(r.result){
                return false;
            }
        }
        return true;
    }
}
