package ua.nure.pivovarenko.SummaryTask4.validate;


public abstract class Validator {

    protected final Errors errors = new Errors();

    public abstract boolean isValid(Object o);

    public Errors getErrors() {
        return errors;
    }
}
