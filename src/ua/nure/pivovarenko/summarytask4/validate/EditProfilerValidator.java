package ua.nure.pivovarenko.SummaryTask4.validate;

import ua.nure.pivovarenko.SummaryTask4.domain.form.UserForm;
import ua.nure.pivovarenko.SummaryTask4.util.ValidationUtil;

import java.util.Objects;

public class EditProfilerValidator extends Validator {

    private static final String LOGIN = "^[a-z0-9_-]{6,20}$";
    private static final String PASSWORD = "[0-9a-zA-Z]{6,}";
    private static final String NAME = "^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \\-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}";

    @Override
    public boolean isValid(Object o) {
        UserForm user = (UserForm)o;
        ValidationUtil.validateValue(errors, user.getLogin(), LOGIN, "error.user.login");
        if(!Objects.equals(user.getPassword(), "")) {
            ValidationUtil.validateValue(errors, user.getPassword(), PASSWORD, "error.user.password");
        }
        ValidationUtil.validateValue(errors, user.getFirstName(),NAME, "error.user.name.first");
        ValidationUtil.validateValue(errors, user.getSecondName(),NAME, "error.user.name.second");
        ValidationUtil.validateValue(errors, user.getMiddleName(),NAME, "error.user.name.middle");
        ValidationUtil.compareValues(errors, user.getPassword(), user.getConformPassword(), "error.user.confirmPassword");
        return errors.hasErrors();
    }
}
