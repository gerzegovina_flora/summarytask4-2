package ua.nure.pivovarenko.SummaryTask4.validate;

import ua.nure.pivovarenko.SummaryTask4.domain.form.EditPatientForm;
import ua.nure.pivovarenko.SummaryTask4.util.ValidationUtil;

import java.util.Objects;

public class EditPatientValidator extends Validator{

    private static final String NAME = "^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \\-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}";
    private static final String PHONE_NUMBER = "^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]?\\d{3}[\\s.-]?\\d{4}$";

    @Override
    public boolean isValid(Object o) {
        EditPatientForm patient = (EditPatientForm)o;
        ValidationUtil.validateValue(errors, patient.getFirstName(), NAME, "error.user.name.first");
        ValidationUtil.validateValue(errors, patient.getSecondName(),NAME, "error.user.name.second");
        ValidationUtil.validateValue(errors, patient.getMiddleName(),NAME, "error.user.name.middle");
        if(!Objects.equals(patient.getPhone(), "")){
            ValidationUtil.validateValue(errors, patient.getPhone(),PHONE_NUMBER, "error.user.phone");
        }
        return errors.hasErrors();
    }
}
