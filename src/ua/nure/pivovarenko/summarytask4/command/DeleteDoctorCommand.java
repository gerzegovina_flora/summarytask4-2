package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteDoctorCommand extends Command{

    private static final Logger LOG = Logger.getLogger(DeleteDoctorCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long id = Long.parseLong(request.getParameter("id"));

        LOG.debug("DOCTOR is being got delete");
        DoctorService doctorService = new DoctorService();
        doctorService.delete(id);
        LOG.debug("DOCTOR has been deleted");
    }
}
