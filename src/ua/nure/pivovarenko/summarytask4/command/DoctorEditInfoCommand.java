package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DoctorEditInfoCommand extends Command {

    private static final Logger LOG = Logger.getLogger(DoctorEditInfoCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long idDoctor = Long.parseLong(request.getParameter("id"));

        LOG.debug("Getting DOCTOR information");
        DoctorService doctorService = new DoctorService();
        Doctor doctor = doctorService.get(idDoctor);
        if(doctor == null){
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            LOG.debug("DOCTOR has not been found");
        }else{
            request.setAttribute("doctor", doctor);
            LOG.debug("DOCTOR has been found");
            request.getRequestDispatcher(Path.PAGE_EDIT_DOCTOR).forward(request,response);
        }
    }
}
