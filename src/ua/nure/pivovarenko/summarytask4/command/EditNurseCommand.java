package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;
import ua.nure.pivovarenko.SummaryTask4.domain.form.EditNurseForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.NurseService;
import ua.nure.pivovarenko.SummaryTask4.validate.EditNurseValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditNurseCommand extends Command {

    private static final Logger LOG = Logger.getLogger(EditNurseCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting NURSE values for updating");

        final String id = request.getParameter("id");
        final String login = request.getParameter("login");
        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String password = request.getParameter("password");
        final String confirmPassword = request.getParameter("confirmPassword");

        EditNurseForm editNurseForm = new EditNurseForm();
        editNurseForm.setId(id);
        editNurseForm.setPassword(password);
        editNurseForm.setFirstName(fName);
        editNurseForm.setMiddleName(mName);
        editNurseForm.setPassword(password);
        editNurseForm.setLogin(login);
        editNurseForm.setConformPassword(confirmPassword);
        editNurseForm.setSecondName(sName);

        Validator userValidator = new EditNurseValidator();

        LOG.debug("Validation NURSE values for updating");
        if(userValidator.isValid(editNurseForm)){
            NurseService nurseService = new NurseService();
            Nurse nurse = editNurseForm.getNurse();
            nurseService.update(nurse);
            LOG.debug("NURSE has been updated");
            response.sendRedirect(request.getContextPath()+"/list.htm?command=nurse_list");
        }else{
            request.setAttribute("errors", userValidator.getErrors());
            LOG.debug("NURSE has not been updated");
            request.getRequestDispatcher(Path.PAGE_EDIT_NURSE).forward(request,response);
        }
    }
}
