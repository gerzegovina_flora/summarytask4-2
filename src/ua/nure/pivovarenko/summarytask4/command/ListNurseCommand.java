package ua.nure.pivovarenko.SummaryTask4.command;


import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.NurseService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListNurseCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        NurseService nurseService = new NurseService();
        request.setAttribute("nurseList", nurseService.getAllNurses());
        request.getRequestDispatcher(Path.PAGE_NURSE_LIST).forward(request,response);
    }
}
