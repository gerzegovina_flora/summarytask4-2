package ua.nure.pivovarenko.SummaryTask4.command;


import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Treatment;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.TreatmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSONAppointComplainTreatmentCommand extends Command{

    private static final Logger LOG = Logger.getLogger(JSONAppointComplainTreatmentCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final String[] descriptions = request.getParameter("descriptions").split(",");
        final Long idHistory = Long.parseLong(request.getParameter("idHistory"));
        final Long idUser = Long.parseLong(request.getParameter("idUser"));

        List<Treatment> treatmentList = new ArrayList<>();

        for (String description : descriptions) {
            Treatment treatment = new Treatment();
            treatment.setDescription(description);
            treatmentList.add(treatment);
            treatment.getElectronicMedicalRecord().getCurrentHistory().setId(idHistory);
            treatment.getUser().setId(idUser);
        }

        TreatmentService treatmentService = new TreatmentService();
        treatmentService.setComplains(treatmentList);
    }
}
