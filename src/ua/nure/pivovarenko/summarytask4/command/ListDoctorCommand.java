package ua.nure.pivovarenko.SummaryTask4.command;


import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListDoctorCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        DoctorService doctorService = new DoctorService();
        request.setAttribute("doctorList", doctorService.getDoctorList());
        request.getRequestDispatcher(Path.PAGE_DOCTOR_LIST).forward(request,response);
    }
}
