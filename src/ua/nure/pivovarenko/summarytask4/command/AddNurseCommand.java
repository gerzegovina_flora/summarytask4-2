package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;
import ua.nure.pivovarenko.SummaryTask4.domain.form.AddNurseForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.NurseService;
import ua.nure.pivovarenko.SummaryTask4.util.ImageConverter;
import ua.nure.pivovarenko.SummaryTask4.validate.AddNurseValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddNurseCommand extends Command{

    private static final Logger LOG = Logger.getLogger(AddNurseCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting NURSE values for creating");

        final String login = request.getParameter("login");
        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String password = request.getParameter("password");
        final String confirmPassword = request.getParameter("confirmPassword");
        final byte [] avatar = ImageConverter.getBytesImage(request.getPart("photo"));

        AddNurseForm addNurseForm = new AddNurseForm();
        addNurseForm.setFirstName(fName);
        addNurseForm.setPassword(password);
        addNurseForm.setSecondName(sName);
        addNurseForm.setConformPassword(confirmPassword);
        addNurseForm.setImage(avatar);
        addNurseForm.setLogin(login);
        addNurseForm.setMiddleName(mName);

        Validator userValidator = new AddNurseValidator();

        LOG.debug("Validation NURSE values for creating");
        if(userValidator.isValid(addNurseForm)){
            NurseService nurseService = new NurseService();
            Nurse nurse = addNurseForm.getNurse();
            nurseService.save(nurse);
            LOG.debug("NURSE has been created");
            response.sendRedirect(request.getContextPath()+"/list.htm?command=nurse_list");
        }else{
            request.setAttribute("errors", userValidator.getErrors());
            LOG.debug("NURSE has not been created");
            request.getRequestDispatcher(Path.PAGE_ADD_NURSE).forward(request,response);
        }
    }
}
