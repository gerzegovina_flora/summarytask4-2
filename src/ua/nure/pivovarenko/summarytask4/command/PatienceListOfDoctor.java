package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class PatienceListOfDoctor extends Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final Long idDoctor = Long.parseLong(request.getParameter("id"));

        PatientService patientService = new PatientService();
        List<Patient> patientList = patientService.getPatientListByDoctorId(idDoctor);

        request.setAttribute("patientList", patientList);

        request.getRequestDispatcher(Path.PAGE_MY_PATIENCE_LIST).forward(request,response);
    }
}
