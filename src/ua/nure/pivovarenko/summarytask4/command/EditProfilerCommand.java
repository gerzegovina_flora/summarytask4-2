package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;
import ua.nure.pivovarenko.SummaryTask4.domain.form.UserForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.UserService;
import ua.nure.pivovarenko.SummaryTask4.validate.EditNurseValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditProfilerCommand extends Command {

    private static final Logger LOG = Logger.getLogger(EditProfilerCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting CURRENT_PROFILE values for updating");

        final String login = request.getParameter("login");
        final String fName = request.getParameter("fName");
        final String id = request.getParameter("id");
        final String password = request.getParameter("password");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String confirmPassword = request.getParameter("confirmPassword");

        UserForm userForm = new UserForm();
        userForm.setId(id);
        userForm.setMiddleName(mName);
        userForm.setLogin(login);
        userForm.setSecondName(sName);
        userForm.setFirstName(fName);
        userForm.setConformPassword(confirmPassword);
        userForm.setPassword(password);

        Validator userValidator = new EditNurseValidator();

        LOG.debug("Validation CURRENT_PROFILE values for updating");
        if(userValidator.isValid(userForm)){
            UserService userService = new UserService();
            User user = (User)userForm.getEntity();
            userService.update(user);
            request.getSession().setAttribute("user", userService.getByLogin(user));
            response.sendRedirect(request.getContextPath() + Path.PAGE_PROFILE);
        }else{
            request.setAttribute("errors", userValidator.getErrors());
            LOG.debug("CURRENT_PROFILE has not been updated");
            request.getRequestDispatcher(Path.PAGE_EDIT_PROFILE).forward(request,response);
        }
    }
}
