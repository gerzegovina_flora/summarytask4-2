package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DischargePatientCommand extends Command {
    private static final Logger LOG = Logger.getLogger(DischargePatientCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long id = Long.parseLong(request.getParameter("id"));
        final long idCurrHist = Long.parseLong(request.getParameter("idCurrHist"));
        final String diagnosis = request.getParameter("diagnosis");

        Patient patient = new Patient();
        patient.setId(id);
        patient.getMedicalRecord().getCurrentHistory().setId(idCurrHist);
        patient.getMedicalRecord().getCurrentHistory().setDiagnosis(diagnosis);

        PatientService patientService = new PatientService();
        patientService.discharge(patient);
        LOG.debug("PATIENT has been discharged");

        response.sendRedirect(request.getHeader("referer"));
    }
}
