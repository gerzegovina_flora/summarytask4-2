package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

/**
 * Holder for all commands.<br/>
 * 
 *
 */
public class CommandContainer {
	
	private static final Logger LOG = Logger.getLogger(CommandContainer.class);
	
	private static Map<String, Command> commands = new TreeMap<>();
	
	static {
		commands.put("login", new LoginCommand());

		commands.put("add_doctor", new AddDoctorCommand());
		commands.put("info_doctor", new DoctorEditInfoCommand());
		commands.put("delete_doctor", new DeleteDoctorCommand());

		commands.put("add_patient", new AddPatientCommand());
		commands.put("add_nurse", new AddNurseCommand());
		commands.put("delete_nurse", new DeleteNurseCommand());

		commands.put("edit_nurse", new EditNurseCommand());
		commands.put("edit_doctor", new EditDoctorCommand());
		commands.put("info_nurse", new NurseEditInfoCommand());
		commands.put("info_patient", new PatientEditInfoCommand());

		commands.put("json_doctor_list", new JSONListDoctorCommand());
		commands.put("doctor_list", new ListDoctorCommand());

		commands.put("nurse_list", new ListNurseCommand());
		commands.put("patient_list", new ListPatientCommand());

		commands.put("patient_info", new PatientInfoCommand());
		commands.put("delete_patient", new DeletePatientCommand());
		commands.put("new_record", new NewRecordHistoryCommand());

		commands.put("complain_t", new JSONAppointComplainTreatmentCommand());
		commands.put("drug_t", new JSONAppointDrugTreatmentCommand());
		commands.put("procedure_t", new JSONAppointProcedureTreatmentCommand());
		commands.put("operation_t", new JSONAppointOperationTreatmentCommand());

		commands.put("sort_doctors", new SortDoctorsCommand());
		commands.put("sort_patients", new SortPatientsCommand());

		commands.put("edit_profile", new EditProfilerCommand());
		commands.put("discharge_p", new DischargePatientCommand());

		commands.put("doc_patience_list", new PatienceListOfDoctor());
		commands.put("logout", new LogoutCommand());
		commands.put("edit_patient", new EditPatientCommand());

		LOG.debug("Command container was successfully initialized");
		LOG.trace("Number of commands --> " + commands.size());
	}

	/**
	 * Returns command object with the given name.
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			LOG.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand"); 
		}
		
		return commands.get(commandName);
	}
	
}