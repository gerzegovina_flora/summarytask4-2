package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.domain.form.AddDoctorForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;
import ua.nure.pivovarenko.SummaryTask4.util.ImageConverter;
import ua.nure.pivovarenko.SummaryTask4.validate.AddDoctorValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddDoctorCommand extends Command {

    private static final Logger LOG = Logger.getLogger(AddDoctorCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting DOCTOR values for creating");

        final String login = request.getParameter("login");
        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String password = request.getParameter("password");
        final String confirmPassword = request.getParameter("confirmPassword");
        final String type = request.getParameter("type");
        final byte [] avatar = ImageConverter.getBytesImage(request.getPart("photo"));

        Validator userValidator = new AddDoctorValidator();

        AddDoctorForm addAddDoctorForm = new AddDoctorForm();
        addAddDoctorForm.setLogin(login);
        addAddDoctorForm.setImage(avatar);
        addAddDoctorForm.setFirstName(fName);
        addAddDoctorForm.setMiddleName(mName);
        addAddDoctorForm.setPassword(password);
        addAddDoctorForm.setConformPassword(confirmPassword);
        addAddDoctorForm.setType(type);
        addAddDoctorForm.setSecondName(sName);

        LOG.debug("Validation DOCTOR values for creating");
        if(userValidator.isValid(addAddDoctorForm)){
            DoctorService doctorService = new DoctorService();
            Doctor doctor = addAddDoctorForm.getEntity();
            doctorService.save(doctor);
            LOG.debug("DOCTOR has been created");
            response.sendRedirect(request.getContextPath()+"/list.htm?command=doctor_list");
        }else{
            request.setAttribute("errors", userValidator.getErrors());
            LOG.debug("DOCTOR has not been created");
            request.getRequestDispatcher(Path.PAGE_ADD_DOCTOR).forward(request,response);
        }
    }
}
