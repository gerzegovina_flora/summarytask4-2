package ua.nure.pivovarenko.SummaryTask4.command;


import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;
import ua.nure.pivovarenko.SummaryTask4.util.JSONUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class JSONListDoctorCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        DoctorService doctorService = new DoctorService();
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        List<Doctor> d = doctorService.getDoctorList();
        response.getWriter().write(JSONUtil.convertToJson(doctorService.getDoctorList()));
    }
}
