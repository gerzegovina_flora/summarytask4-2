package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Treatment;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.TreatmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JSONAppointProcedureTreatmentCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final String[] drugsName = request.getParameter("procedure-name").split(",");
        final String[] days = request.getParameter("days").split(",");
        final String[] descriptions = request.getParameter("descriptions").split(",");
        final Long idHistory = Long.parseLong(request.getParameter("idHistory"));
        final Long idUser = Long.parseLong(request.getParameter("idUser"));

        List<Treatment> treatmentList = new ArrayList<>();

        for(int i = 0; i < drugsName.length; i++){
            Treatment treatment = new Treatment();
            treatment.setDescription(descriptions[i]);
            treatment.setText(drugsName[i]);
            treatment.setDays(days[i]);
            treatmentList.add(treatment);
            treatment.getElectronicMedicalRecord().getCurrentHistory().setId(idHistory);
            treatment.getUser().setId(idUser);
        }

        TreatmentService treatmentService = new TreatmentService();
        treatmentService.appointProcedures(treatmentList);
    }
}
