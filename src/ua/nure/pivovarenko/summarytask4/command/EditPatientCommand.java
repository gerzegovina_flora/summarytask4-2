package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.domain.form.EditPatientForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;
import ua.nure.pivovarenko.SummaryTask4.validate.EditPatientValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditPatientCommand extends Command {

    private static final Logger LOG = Logger.getLogger(EditPatientCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting PATIENT values for updating");

        final String id = request.getParameter("id");
        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String mobile = request.getParameter("phone");
        final String work = request.getParameter("work");
        final String address = request.getParameter("address");

        EditPatientForm patientForm = new EditPatientForm();
        patientForm.setFirstName(fName);
        patientForm.setAddress(address);
        patientForm.setPhone(mobile);
        patientForm.setId(id);
        patientForm.setWork(work);
        patientForm.setSecondName(sName);
        patientForm.setMiddleName(mName);

        Validator patientValidator = new EditPatientValidator();

        LOG.debug("Validation PATIENT values for updating");
        if(patientValidator.isValid(patientForm)){
            PatientService patientService = new PatientService();
            Patient patient = patientForm.getEntity();
            patientService.update(patient);
            LOG.debug("PATIENT has been updated");
            response.sendRedirect(request.getContextPath()+"/list.htm?command=patient_list");
        }else{
            request.setAttribute("errors", patientValidator.getErrors());
            LOG.debug("PATIENT has not been updated");
            request.getRequestDispatcher(Path.PAGE_EDIT_PATIENT).forward(request,response);
        }
    }
}
