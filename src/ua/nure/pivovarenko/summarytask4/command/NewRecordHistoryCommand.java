package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NewRecordHistoryCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long idMedRec = Long.parseLong(request.getParameter("idMedRec"));
        final long idPat = Long.parseLong(request.getParameter("id"));
        final long idDoc = Long.parseLong(request.getParameter("doctor"));

        Patient patient = new Patient();
        patient.setId(idPat);
        patient.getMedicalRecord().setId(idMedRec);
        patient.getMedicalRecord().getCurrentHistory().getDoctor().setId(idDoc);

        PatientService patientService = new PatientService();
        patientService.newHistoryRecord(patient);

        response.sendRedirect(request.getHeader("referer"));
    }
}
