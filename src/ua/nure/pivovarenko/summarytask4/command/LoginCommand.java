package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.db.dao.PatientDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class LoginCommand extends Command {

    private static final Logger LOG = Logger.getLogger(LoginCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final User user = new User();
        final UserService userService = new UserService();

        user.setPassword(request.getParameter("password"));
        user.setLogin(request.getParameter("login"));

        User authUser = userService.get(user);

        if(authUser != null){
            LOG.debug("USER "+user+" has been authenticated");
            HttpSession session = request.getSession();
            session.setAttribute("user", authUser);
            response.sendRedirect(request.getContextPath()+Path.PAGE_PROFILE);

            PatientDao patientDao = new PatientDao();
            List<Patient> patientList = patientDao.getPatientsByDrugs();
            request.getSession().setAttribute("mostDrugs", patientList);

        }else{
            LOG.debug("USER "+user+" has not been authenticated");
            response.sendRedirect(request.getContextPath()+"/index.jsp?error=auth");
        }
    }
}
