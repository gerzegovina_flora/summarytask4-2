package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;
import ua.nure.pivovarenko.SummaryTask4.util.SortCollection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SortPatientsCommand extends Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        String order = request.getParameter("by");

        PatientService patientService = new PatientService();
        List<Patient> patients = patientService.getPatientList();

        SortCollection.sort(patients, order);

        request.setAttribute("patientList", patients);
        request.getRequestDispatcher(Path.PAGE_PATIENCE_LIST).forward(request, response);
    }
}
