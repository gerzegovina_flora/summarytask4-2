package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.domain.form.EditDoctorForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;
import ua.nure.pivovarenko.SummaryTask4.validate.EditDoctorValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditDoctorCommand extends Command {

    private static final Logger LOG = Logger.getLogger(EditDoctorCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting DOCTOR values for updating");

        final String id = request.getParameter("id");
        final String login = request.getParameter("login");
        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String password = request.getParameter("password");
        final String confirmPassword = request.getParameter("confirmPassword");

        Validator userValidator = new EditDoctorValidator();

        EditDoctorForm editDoctorForm = new EditDoctorForm();
        editDoctorForm.setId(id);
        editDoctorForm.setLogin(login);
        editDoctorForm.setFirstName(fName);
        editDoctorForm.setMiddleName(mName);
        editDoctorForm.setPassword(password);
        editDoctorForm.setConformPassword(confirmPassword);
        editDoctorForm.setSecondName(sName);

        LOG.debug("Validation DOCTOR values for updating");
        if(userValidator.isValid(editDoctorForm)){
            DoctorService doctorService = new DoctorService();
            Doctor doctor = editDoctorForm.getEntity();
            doctorService.update(doctor);
            LOG.debug("DOCTOR has been updated");
            response.sendRedirect(request.getContextPath()+"/list.htm?command=doctor_list");
        }else{
            request.setAttribute("errors", userValidator.getErrors());
            LOG.debug("DOCTOR has not been updated");
            request.getRequestDispatcher(Path.PAGE_EDIT_DOCTOR).forward(request, response);
        }
    }
}
