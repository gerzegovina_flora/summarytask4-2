package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.domain.form.AddPatientForm;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;
import ua.nure.pivovarenko.SummaryTask4.util.ImageConverter;
import ua.nure.pivovarenko.SummaryTask4.validate.AddPatientValidator;
import ua.nure.pivovarenko.SummaryTask4.validate.Validator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddPatientCommand extends Command{

    private static final Logger LOG = Logger.getLogger(AddPatientCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        LOG.debug("Getting PATIENT values for creating");

        final String fName = request.getParameter("fName");
        final String sName = request.getParameter("sName");
        final String mName = request.getParameter("mName");
        final String gender = request.getParameter("gender");
        final String blood = request.getParameter("blood");
        final String birthday = request.getParameter("birthday");
        final String work = request.getParameter("work");
        final String address = request.getParameter("address");
        final String phone = request.getParameter("phone");
        final byte [] avatar = ImageConverter.getBytesImage(request.getPart("photo"));
        final String idDoctor = request.getParameter("doctor");
        final String registryDate = request.getParameter("registryDate");

        AddPatientForm addPatientForm = new AddPatientForm();
        addPatientForm.setFirstName(fName);
        addPatientForm.setSecondName(sName);
        addPatientForm.setMiddleName(mName);
        addPatientForm.setGender(gender);
        addPatientForm.setBlood(blood);
        addPatientForm.setBirthday(birthday);
        addPatientForm.setWork(work);
        addPatientForm.setAddress(address);
        addPatientForm.setPhone(phone);
        addPatientForm.setImage(avatar);
        addPatientForm.setIdDoctor(idDoctor);
        addPatientForm.setRegistryDate(registryDate);

        Validator patientValidator = new AddPatientValidator();

        LOG.debug("Validation PATIENT values for creating");

        if(patientValidator.isValid(addPatientForm)){
            PatientService patientService = new PatientService();
            Patient patient = addPatientForm.getEntity();
            patientService.save(patient);
            LOG.debug("PATIENT has been created");
            response.sendRedirect(request.getContextPath()+"/patient-list.htm?command=patient_list");
        }else {
            request.setAttribute("errors", patientValidator.getErrors());
            LOG.debug("DOCTOR has not been created");
            request.getRequestDispatcher(Path.PAGE_ADD_PATIENT).forward(request,response);
        }
    }
}
