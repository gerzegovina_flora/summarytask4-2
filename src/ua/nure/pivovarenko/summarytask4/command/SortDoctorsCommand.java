package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.DoctorService;
import ua.nure.pivovarenko.SummaryTask4.util.SortCollection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SortDoctorsCommand extends Command{

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final String order = request.getParameter("by");

        DoctorService doctorService = new DoctorService();
        List<Doctor> doctorList = doctorService.getDoctorList();

        SortCollection.sort(doctorList, order);

        request.setAttribute("doctorList", doctorList);
        request.getRequestDispatcher(Path.PAGE_DOCTOR_LIST).forward(request,response);
    }
}
