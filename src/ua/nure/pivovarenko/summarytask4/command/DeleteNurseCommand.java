package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.NurseService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteNurseCommand extends Command{

    private static final Logger LOG = Logger.getLogger(DeleteNurseCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long id = Long.parseLong(request.getParameter("id"));

        LOG.debug("NURSE is being got delete");
        NurseService nurseService = new NurseService();
        nurseService.delete(id);
        LOG.debug("NURSE has been deleted");
    }
}
