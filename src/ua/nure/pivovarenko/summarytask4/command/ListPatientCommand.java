package ua.nure.pivovarenko.SummaryTask4.command;


import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ListPatientCommand extends Command{
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        PatientService patientService = new PatientService();
        request.setAttribute("patientList", patientService.getPatientList());
        request.getRequestDispatcher(Path.PAGE_PATIENCE_LIST).forward(request,response);
    }
}
