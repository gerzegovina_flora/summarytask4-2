package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PatientEditInfoCommand extends Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long idPatient = Long.parseLong(request.getParameter("id"));

        PatientService patientService = new PatientService();
        Patient patient = patientService.getPatientForEdit(idPatient);
        if(patient == null){
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }else{
            request.setAttribute("patient", patient);
            request.getRequestDispatcher(Path.PAGE_EDIT_PATIENT).forward(request,response);
        }
    }
}
