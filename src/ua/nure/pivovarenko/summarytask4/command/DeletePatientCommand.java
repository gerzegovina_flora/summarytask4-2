package ua.nure.pivovarenko.SummaryTask4.command;

import org.apache.log4j.Logger;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeletePatientCommand extends Command {

    private static final Logger LOG = Logger.getLogger(DeletePatientCommand.class);
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final Long idPatient = Long.parseLong(request.getParameter("idPatient"));
        final Long idMedRec = Long.parseLong(request.getParameter("idMedRec"));

        Patient patient = new Patient();
        patient.setId(idPatient);
        patient.getMedicalRecord().getCurrentHistory().setId(idMedRec);

        LOG.debug("PATIENT is being got delete");
        PatientService patientService = new PatientService();
        patientService.delete(patient);
        LOG.debug("PATIENT has been deleted");
    }
}
