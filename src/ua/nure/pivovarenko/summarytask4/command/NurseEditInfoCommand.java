package ua.nure.pivovarenko.SummaryTask4.command;

import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import ua.nure.pivovarenko.SummaryTask4.service.NurseService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NurseEditInfoCommand extends Command {
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, AppException {
        final long idNurse = Long.parseLong(request.getParameter("id"));

        NurseService nurseService = new NurseService();
        Nurse nurse = nurseService.get(idNurse);
        if(nurse == null){
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }else{
            request.setAttribute("nurse", nurse);
            request.getRequestDispatcher(Path.PAGE_EDIT_NURSE).forward(request,response);
        }
    }
}
