package ua.nure.pivovarenko.SummaryTask4.domain;

public enum BloodType {
    O_P, O_M,
    A_P, A_M,
    B_P, B_M,
    AB_P, AB_M
}
