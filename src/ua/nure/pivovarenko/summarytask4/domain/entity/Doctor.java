package ua.nure.pivovarenko.SummaryTask4.domain.entity;

import ua.nure.pivovarenko.SummaryTask4.domain.DoctorType;

import java.util.ArrayList;
import java.util.List;

public class Doctor extends User {

    private DoctorType type;
    private List<Patient> patientList = new ArrayList<>();

    public List<Patient> getPatientList() {
        return patientList;
    }

    public DoctorType getType() {
        return type;
    }

    public void setType(DoctorType type) {
        this.type = type;
    }
}
