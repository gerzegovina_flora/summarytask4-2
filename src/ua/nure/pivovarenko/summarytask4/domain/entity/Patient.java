package ua.nure.pivovarenko.SummaryTask4.domain.entity;


import java.sql.Date;

public class Patient extends User{

    private Date birthday;
    private String gender;
    private ElectronicMedicalRecord medicalRecord = new ElectronicMedicalRecord();
    private Boolean discharged;
    private String work;
    private String phone;
    private String address;

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getDischarged() {
        return discharged;
    }

    public void setDischarged(Boolean discharged) {
        this.discharged = discharged;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public ElectronicMedicalRecord getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(ElectronicMedicalRecord medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
