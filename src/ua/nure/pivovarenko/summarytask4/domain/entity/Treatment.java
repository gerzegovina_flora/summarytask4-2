package ua.nure.pivovarenko.SummaryTask4.domain.entity;

public class Treatment extends Entity{

    private User user = new User();
    private String text;
    private ElectronicMedicalRecord electronicMedicalRecord = new ElectronicMedicalRecord();
    private String description;
    private String days;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public ElectronicMedicalRecord getElectronicMedicalRecord() {
        return electronicMedicalRecord;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
