package ua.nure.pivovarenko.SummaryTask4.domain.entity;

import org.apache.commons.codec.binary.Base64;

import java.io.Serializable;

public class Image implements Serializable {

    private byte[] bytesImage;
    private String imageBase64;

    public byte[] getBytesImage() {
        return bytesImage;
    }

    public void setImage(byte[] image) {
        imageBase64 = "data:image/jpg;base64,".concat(new String(new Base64().encode(image)));
        this.bytesImage = image;
    }

    public String getImageBase64() {
        return imageBase64;
    }
}