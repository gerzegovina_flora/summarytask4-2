package ua.nure.pivovarenko.SummaryTask4.domain.entity;

import ua.nure.pivovarenko.SummaryTask4.domain.BloodType;

import java.util.ArrayList;
import java.util.List;

public class ElectronicMedicalRecord extends Entity{

    private List<History> histories = new ArrayList<>();
    private History currentHistory = new History();
    private BloodType bloodType;

    public History getCurrentHistory() {
        return currentHistory;
    }

    public void setCurrentHistory(History currentHistory) {
        this.currentHistory = currentHistory;
    }

    public List<History> getHistories() {
        return histories;
    }

    public void setHistories(List<History> histories) {
        this.histories = histories;
    }

    public BloodType getBloodType() {
        return bloodType;
    }

    public void setBloodType(BloodType bloodType) {
        this.bloodType = bloodType;
    }
}
