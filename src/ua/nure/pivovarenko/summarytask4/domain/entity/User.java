package ua.nure.pivovarenko.SummaryTask4.domain.entity;

import ua.nure.pivovarenko.SummaryTask4.domain.Role;

public class User extends Entity{

    private String firstName;
    private String secondName;
    private String middleName;
    private String login;
    private Role role;
    private String password;
    private Image image = new Image();

    public Image getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image.setImage(image);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                ", password='" + password + '\'' +
                ", image=" + image +
                '}';
    }
}
