package ua.nure.pivovarenko.SummaryTask4.domain.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class History {
    private Long id;
    private Date registryDate;
    private Boolean closed;
    private String diagnosis;
    private Doctor doctor = new Doctor();
    private List<Treatment> drugs = new ArrayList<>();
    private List<Treatment> operations = new ArrayList<>();
    private List<Treatment> complains = new ArrayList<>();
    private List<Treatment> procedures = new ArrayList<>();

    public List<Treatment> getOperations() {
        return operations;
    }

    public void setOperations(List<Treatment> operations) {
        this.operations = operations;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public List<Treatment> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<Treatment> drugs) {
        this.drugs = drugs;
    }

    public List<Treatment> getComplains() {
        return complains;
    }

    public void setComplains(List<Treatment> complains) {
        this.complains = complains;
    }

    public List<Treatment> getProcedures() {
        return procedures;
    }

    public void setProcedures(List<Treatment> procedures) {
        this.procedures = procedures;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }
}
