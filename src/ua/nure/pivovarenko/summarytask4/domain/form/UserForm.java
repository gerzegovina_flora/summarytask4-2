package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;

public class UserForm implements Form{

    protected String id;
    protected String firstName;
    protected String secondName;
    protected String middleName;
    protected String login;
    protected String password;
    protected String conformPassword;
    protected byte[] image = new byte[0];

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConformPassword() {
        return conformPassword;
    }

    public void setConformPassword(String conformPassword) {
        this.conformPassword = conformPassword;
    }

    @Override
    public Object getEntity() {
        User user = new User();
        user.setLogin(login);
        user.setFirstName(firstName);
        user.setId(Long.parseLong(id));
        user.setPassword(password);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        user.setImage(image);
        return user;
    }
}
