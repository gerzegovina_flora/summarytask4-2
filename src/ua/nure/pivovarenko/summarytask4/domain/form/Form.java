package ua.nure.pivovarenko.SummaryTask4.domain.form;

public interface Form {

    Object getEntity();

}
