package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.DoctorType;
import ua.nure.pivovarenko.SummaryTask4.domain.Role;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;

public class AddDoctorForm extends UserForm {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Doctor getEntity() {
        Doctor doctor = new Doctor();
        doctor.setLogin(login);
        doctor.setFirstName(firstName);
        doctor.setSecondName(secondName);
        doctor.setMiddleName(middleName);
        doctor.setPassword(password);
        doctor.setImage(image);
        doctor.setRole(Role.DOCTOR);
        doctor.setType(DoctorType.valueOf(type));
        return doctor;
    }
}
