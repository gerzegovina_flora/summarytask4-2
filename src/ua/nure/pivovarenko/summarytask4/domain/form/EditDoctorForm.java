package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;

public class EditDoctorForm extends UserForm {

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Doctor getEntity() {
        Doctor doctor = new Doctor();
        doctor.setId(Long.parseLong(id));
        doctor.setLogin(login);
        doctor.setFirstName(firstName);
        doctor.setSecondName(secondName);
        doctor.setMiddleName(middleName);
        doctor.setPassword(password);
        return doctor;
    }
}
