package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;

public class EditNurseForm extends UserForm {

    public Nurse getNurse(){
        Nurse nurse = new Nurse();
        nurse.setId(Long.parseLong(id));
        nurse.setLogin(login);
        nurse.setSecondName(secondName);
        nurse.setPassword(password);
        nurse.setMiddleName(middleName);
        nurse.setFirstName(firstName);
        return nurse;
    }

}
