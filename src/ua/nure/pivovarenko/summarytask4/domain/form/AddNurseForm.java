package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.Role;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;

public class AddNurseForm extends UserForm {

    public Nurse getNurse(){
        Nurse nurse = new Nurse();
        nurse.setLogin(login);
        nurse.setSecondName(secondName);
        nurse.setPassword(password);
        nurse.setMiddleName(middleName);
        nurse.setImage(image);
        nurse.setFirstName(firstName);
        nurse.setRole(Role.NURSE);
        return nurse;
    }

}
