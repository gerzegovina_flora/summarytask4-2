package ua.nure.pivovarenko.SummaryTask4.domain.form;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;

public class EditPatientForm extends UserForm {

    private String birthday;
    private String gender;
    private String phone;
    private String work;
    private String address;
    private String blood;
    private String idDoctor;
    private String registryDate;

    public String getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(String registryDate) {
        this.registryDate = registryDate;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(String idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public Patient getEntity() {
        Patient patient = new Patient();
        patient.setId(Long.parseLong(id));
        patient.setFirstName(firstName);
        patient.setSecondName(secondName);
        patient.setMiddleName(middleName);
        patient.setWork(work);
        patient.setAddress(address);
        patient.setPhone(phone);
        return patient;
    }

}
