package ua.nure.pivovarenko.SummaryTask4.service;

import ua.nure.pivovarenko.SummaryTask4.db.dao.DAOException;
import ua.nure.pivovarenko.SummaryTask4.db.dao.UserDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;

public class UserService {

    private final UserDao userDao = new UserDao();

    public User get(User user){
        User u = null;
        try {
            u = userDao.get(user);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return u;
    }

    public User getByLogin(User user){
        User u = null;
        try {
            u = userDao.getByLogin(user);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return u;
    }

    public void update(User user){
        userDao.update(user);
    }

}
