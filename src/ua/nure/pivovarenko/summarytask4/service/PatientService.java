package ua.nure.pivovarenko.SummaryTask4.service;

import ua.nure.pivovarenko.SummaryTask4.db.dao.PatientDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;

import java.util.List;

public class PatientService {

    private final PatientDao patientDao = new PatientDao();

    public void save(Patient patient){
        patientDao.save(patient);
    }

    public void newHistoryRecord(Patient patient){
        patientDao.newHistoryRecord(patient);
    }

    public void delete(Patient patient){patientDao.delete(patient);}
    public void discharge(Patient patient){patientDao.discharge(patient);}

    public List<Patient> getPatientList(){
        return patientDao.getPatientList();
    }
    public List<Patient> getPatientListByDoctorId(long id){
        return patientDao.getPatientListByDoctorId(id);
    }

    public Patient get(long id){
        return patientDao.get(id);
    }
    public Patient getPatientForEdit(long id){
        return patientDao.getPatientForEdit(id);
    }

    public void update(Patient patient){
        patientDao.update(patient);
    }

}
