package ua.nure.pivovarenko.SummaryTask4.service;

import ua.nure.pivovarenko.SummaryTask4.db.dao.DoctorDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;

import java.util.List;

public class DoctorService {

    private final DoctorDao doctorDao = new DoctorDao();

    public List<Doctor> getAllDoctorTypes(){
        return doctorDao.getDoctorTypes();
    }

    public List<Doctor> getDoctorsByType(Long id){
        return doctorDao.getDoctorsByType(id);
    }

    public void save(Doctor doctor){
        doctorDao.save(doctor);
    }

    public List<Doctor> getDoctorList(){
        return doctorDao.getDoctorList();
    }

    public void delete(long id){
        doctorDao.delete(id);
    }

    public Doctor get(Long id){
        return doctorDao.get(id);
    }

    public void update(Doctor doctor){
        doctorDao.update(doctor);
    }

}
