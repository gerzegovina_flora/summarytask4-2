package ua.nure.pivovarenko.SummaryTask4.service;

import ua.nure.pivovarenko.SummaryTask4.db.dao.NurseDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;

import java.util.List;


public class NurseService {

    private final NurseDao nurseDao = new NurseDao();

    public void save(Nurse nurse){
        nurseDao.save(nurse);
    }

    public void update(Nurse nurse){
        nurseDao.update(nurse);
    }

    public void delete(long id){
        nurseDao.delete(id);
    }

    public List<Nurse> getAllNurses(){
        return nurseDao.getAllNurses();
    }

    public Nurse get(Long id){
        return nurseDao.get(id);
    }

}
