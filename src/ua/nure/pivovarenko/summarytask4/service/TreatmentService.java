package ua.nure.pivovarenko.SummaryTask4.service;

import ua.nure.pivovarenko.SummaryTask4.db.dao.TreatmentDao;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Treatment;

import java.util.List;

public class TreatmentService {

    private final TreatmentDao treatmentDao = new TreatmentDao();

    public void appointDrugs(List<Treatment> treatmentList){
        treatmentDao.appointDrugs(treatmentList);
    }

    public void appointProcedures(List<Treatment> treatmentList){
        treatmentDao.appointProcedures(treatmentList);
    }

    public void setComplains(List<Treatment> treatmentList){
        treatmentDao.setComplains(treatmentList);
    }

    public void appointOperations(List<Treatment> treatmentList){
        treatmentDao.appointOperations(treatmentList);
    }

}
