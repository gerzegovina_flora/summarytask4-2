package ua.nure.pivovarenko.SummaryTask4.util;

import org.apache.commons.io.IOUtils;

import javax.servlet.http.Part;
import java.io.IOException;

public final class ImageConverter {

    private ImageConverter(){}

    public static byte[] getBytesImage(Part part) throws IOException {
        byte[] bytes = null;
        if(part != null){
            bytes = IOUtils.toByteArray(part.getInputStream());
        }
        return bytes;
    }

}
