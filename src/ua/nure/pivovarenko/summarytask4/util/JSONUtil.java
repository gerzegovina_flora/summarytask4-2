package ua.nure.pivovarenko.SummaryTask4.util;

import com.google.gson.Gson;

import java.util.List;

public final class JSONUtil {

    private JSONUtil(){}

    public static String convertToJson(List<?> list){
        return new Gson().toJson(list);
    }

}
