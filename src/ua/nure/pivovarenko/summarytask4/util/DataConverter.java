package ua.nure.pivovarenko.SummaryTask4.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class DataConverter {

    private DataConverter(){}

    public static Date getDate(String date) {
        DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
        java.util.Date $date = new java.util.Date(0);
        try {
            $date = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date($date.getTime());
    }

}
