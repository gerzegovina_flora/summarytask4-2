package ua.nure.pivovarenko.SummaryTask4.util;

import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;

import java.util.*;

public final class SortCollection {

    private static final Comparator<?> SORT_PATIENTS_BY_NAME = new Comparator<Patient>() {
        @Override
        public int compare(Patient o1, Patient o2) {
            return o1.getFirstName().compareTo(o2.getFirstName());
        }
    };

    private static final Comparator<?> SORT_PATIENTS_BY_BIRTHDAY = new Comparator<Patient>() {
        @Override
        public int compare(Patient o1, Patient o2) {
            return o1.getBirthday().compareTo(o2.getBirthday());
        }
    };

    private static final Comparator<?> SORT_DOCTORS_BY_PATIENT_COUNT = new Comparator<Doctor>() {
        @Override
        public int compare(Doctor o1, Doctor o2) {
            return o2.getPatientList().size() - o1.getPatientList().size();
        }
    };

    private static final Comparator<?> SORT_DOCTORS_BY_NAME = new Comparator<Doctor>() {
        @Override
        public int compare(Doctor o1, Doctor o2) {
            return o1.getFirstName().compareTo(o2.getFirstName());
        }
    };

    private static final Comparator<?> SORT_DOCTORS_BY_TYPE = new Comparator<Doctor>() {
        @Override
        public int compare(Doctor o1, Doctor o2) {
            return o1.getType().compareTo(o2.getType());
        }
    };

    private static final Map<String, Comparator> SORTED_MAP = new TreeMap<String, Comparator>(){{
        put("P_ALPHABET", SORT_PATIENTS_BY_NAME);
        put("BIRTHDAY", SORT_PATIENTS_BY_BIRTHDAY);
        put("PATIENT_COUNT", SORT_DOCTORS_BY_PATIENT_COUNT);
        put("D_ALPHABET", SORT_DOCTORS_BY_NAME);
        put("TYPE", SORT_DOCTORS_BY_TYPE);
    }};

    public static void sort(List<?> list, String order){
        if(list != null && list.size() > 0 && SORTED_MAP.containsKey(order)){
            Collections.sort(list, SORTED_MAP.get(order));
        }else {
            throw new IllegalArgumentException("Arguments are not proper");
        }
    }

}
