package ua.nure.pivovarenko.SummaryTask4.util;

import ua.nure.pivovarenko.SummaryTask4.validate.Errors;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidationUtil {
    private ValidationUtil(){}

    public static void validateValue(Errors errors, String value, String rule, String key){
        Pattern pattern = Pattern.compile(rule);
        if(value != null && !value.equals("")){
            Matcher matcher = pattern.matcher(value);
            if(!matcher.matches())
                errors.setError(key,value,true);
            else{
                errors.setError(key,value,false);
            }
        }else{
            errors.setError(key, value, true);
        }
    }

    public static void compareValues(Errors errors, String value1, String value2, String key){
        if (value1 != null && value2 != null && !value1.equals(value2))
            errors.setError(key, key, true);
        else
            errors.setError(key,key,false);
    }

}
