package ua.nure.pivovarenko.SummaryTask4.web.listener;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ContextListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;

        PropertyConfigurator.configure(fullPath);

        String localesFileName = context.getInitParameter("locales");
        String localesFileRealPath = context.getRealPath(localesFileName);

        Properties locales = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(localesFileRealPath);
            locales.load(fis);
        } catch (IOException e) {
            LOG.error("Exception load locales", e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    LOG.error("Error load locales");
                }
            }
        }
        context.setAttribute("locales", locales);
        locales.list(System.out);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    }
}