package ua.nure.pivovarenko.SummaryTask4.web.controller;

import ua.nure.pivovarenko.SummaryTask4.command.Command;
import ua.nure.pivovarenko.SummaryTask4.command.CommandContainer;
import ua.nure.pivovarenko.SummaryTask4.exception.AppException;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BasicServlet extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(BasicServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req,resp);
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        LOG.debug("Controller starts");

        // extract command name from the request
        String commandName = req.getParameter("command");
        LOG.trace("Request parameter: command --> " + commandName);

        if(commandName != null) {
            // obtain command object by its name
            Command command = CommandContainer.get(commandName);
            LOG.trace("Obtained command --> " + command);

            // execute command and get forward address
            try {
                command.execute(req, resp);
            } catch (AppException ex) {
                req.setAttribute("errorMessage", ex.getMessage());
            }
            LOG.trace("Forward address --> " /*+ forward*/);
        }
    }
}
