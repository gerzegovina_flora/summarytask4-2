package ua.nure.pivovarenko.SummaryTask4.web.tag;

import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class SelectTag extends SimpleTagSupport {

    private String classUrl;
    private String parameter;
    private String name;
    private String localePrefix;

    private static final Logger LOG = Logger.getLogger(SelectTag.class);

    public void setName(String name) {
        this.name = name;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public void setClassUrl(String classUrl) {
        this.classUrl = classUrl;
    }

    public void setLocalePrefix(String localePrefix) {
        this.localePrefix = localePrefix;
    }

    @Override
    public void doTag() throws JspException, IOException {
        String lang = (String) getJspContext().getAttribute("currentLocale", PageContext.SESSION_SCOPE);
        Locale locale;
        if (lang == null) {
            locale = Locale.getDefault();
        } else {
            locale = new Locale(lang);
        }
        ResourceBundle resourceBundle = ResourceBundle.getBundle("resources", locale);

        JspWriter out = getJspContext().getOut();
        Object[] values = null;
        try {
            Class<?> e = Class.forName(classUrl);
            values = e.getEnumConstants();
        } catch (ClassNotFoundException e) {
            LOG.error("Class not found", e);
        }
        out.println("<select name=" + name + " class=\"form-control\" tabindex=\"-1\">");
        for (Object at : values) {
            if (!at.toString().equals("ADMIN")) {
                out.println("<option value=" + at.toString() + " " + (parameter != null && (parameter.equals(at.toString())) ? "selected" : "") + ">" + resourceBundle.getString(localePrefix + "." + at.toString().toLowerCase().replaceAll("_", "")) + "</option>");
            }
        }
        out.println("</select>");
    }
}