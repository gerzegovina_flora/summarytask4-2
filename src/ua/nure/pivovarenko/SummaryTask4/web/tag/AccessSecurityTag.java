package ua.nure.pivovarenko.SummaryTask4.web.tag;


import ua.nure.pivovarenko.SummaryTask4.Path;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class AccessSecurityTag extends TagSupport {

    private String hasRole;
    private String except;

    public String getHasRole() {
        return hasRole;
    }

    public void setHasRole(String hasRole) {
        this.hasRole = hasRole;
    }

    public String getExcept() {
        return except;
    }

    public void setExcept(String except) {
        this.except = except;
    }

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        User user = (User) session.getAttribute("user");

        List<String> roles = Arrays.asList(hasRole.split(","));

        HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

        try {
            if(user != null){
                if(!roles.contains(user.getRole().toString())){
                    response.sendRedirect(request.getContextPath() + Path.PAGE_PROFILE);
                }
            }else{
                response.sendRedirect(request.getContextPath() + Path.PAGE_INDEX);
            }
        } catch (IOException e) {
            throw new JspException("Permit fail", e);
        }
        return super.doStartTag();
    }

}
