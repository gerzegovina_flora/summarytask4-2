package ua.nure.pivovarenko.SummaryTask4.db.dao;

import ua.nure.pivovarenko.SummaryTask4.db.util.DBUtil;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NurseDao {

    private static final String SAVE_NURSE = "INSERT INTO \"USER\"(login, \"name\", role, password, image, last_name, middle_name) VALUES (?, ?, ?, md5(?), [?],?,?)";
    private static final String UPDATE_NURSE = "UPDATE \"USER\" SET login = ?, name = ?, password = md5(?), last_name = ?, middle_name = ? WHERE id = ?";
    private static final String UPDATE_NURSE_WITHOUT_PASSWORD = "UPDATE \"USER\" SET login = ?, name = ?, last_name = ?, middle_name = ? WHERE id = ?";
    private static final String DELETE_NURSE = "DELETE FROM \"USER\" WHERE id = ?";
    private static final String GET_ALL_NURSES = "select * from \"NURSE\" n , \"USER\" u WHERE n.id_user = u.\"id\"";
    private static final String GET_NURSE = "select * from \"NURSE\" n , \"USER\" u WHERE n.id_user = u.\"id\" and n.id_user = ?";

    public void delete(long id){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        try{
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(DELETE_NURSE);
            preparedStatement1.setLong(1, id);
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public void save(Nurse nurse){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        ResultSet rs = null;
        try{
            connection.setAutoCommit(false);
            boolean isImage = nurse.getImage().getBytesImage().length > 0;
            String save = SAVE_NURSE;
            if(isImage){
                save = save.replace("[?]", "?");
            }else{
                save = save.replace("[?]", "default");
            }
            preparedStatement1 = connection.prepareStatement(save, PreparedStatement.RETURN_GENERATED_KEYS);
            int index = 1;
            preparedStatement1.setString(index++, nurse.getLogin());
            preparedStatement1.setString(index++, nurse.getFirstName());
            preparedStatement1.setInt(index++, nurse.getRole().ordinal());
            preparedStatement1.setString(index++, nurse.getPassword());
            if(isImage) {
                preparedStatement1.setBytes(index++, nurse.getImage().getBytesImage());
            }
            preparedStatement1.setString(index++, nurse.getSecondName());
            preparedStatement1.setString(index, nurse.getMiddleName());
            preparedStatement1.executeUpdate();
            rs = preparedStatement1.getGeneratedKeys();
            long idUser = 0;
            if (rs.next()) {
                idUser = rs.getLong(1);
            }
            preparedStatement2 = connection.prepareStatement("INSERT INTO \"NURSE\" (id_user) VALUES (?)");
            preparedStatement2.setLong(1, idUser);
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public void update(Nurse nurse){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        try{
            connection.setAutoCommit(false);
            boolean password = !Objects.equals(nurse.getPassword(), "");
            if(password)
                preparedStatement1 = connection.prepareStatement(UPDATE_NURSE);
            else
                preparedStatement1 = connection.prepareStatement(UPDATE_NURSE_WITHOUT_PASSWORD);
            int index = 1;
            preparedStatement1.setString(index++, nurse.getLogin());
            preparedStatement1.setString(index++, nurse.getFirstName());
            if(password){
                preparedStatement1.setString(index++, nurse.getPassword());
            }
            preparedStatement1.setString(index++, nurse.getSecondName());
            preparedStatement1.setString(index++, nurse.getMiddleName());
            preparedStatement1.setLong(index++, nurse.getId());
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public List<Nurse> getAllNurses(){
        List<Nurse> nurseList = null;
        Connection connection = PostgresConnection.getConnection();
        Statement statement = null;
        ResultSet rs = null;
        try{
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_NURSES);
            nurseList = new ArrayList<>();
            while (rs.next()){
                Nurse nurse = new Nurse();
                nurse.setId(rs.getLong("id_user"));
                nurse.setFirstName(rs.getString("name"));
                nurse.setLogin(rs.getString("login"));
                nurse.setPassword(rs.getString("password"));
                nurse.getImage().setImage(rs.getBytes("image"));
                nurseList.add(nurse);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(statement);
            //DBUtil.close(rs);
        }

        return nurseList;
    }

    public Nurse get(Long id){
        Nurse nurse = null;
        Connection connection = PostgresConnection.getConnection();
        ResultSet rs = null;
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(GET_NURSE);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()){
                nurse = new Nurse();
                nurse.setId(rs.getLong("id_user"));
                nurse.setFirstName(rs.getString("name"));
                nurse.setLogin(rs.getString("login"));
                nurse.setPassword(rs.getString("password"));
                nurse.getImage().setImage(rs.getBytes("image"));
                nurse.setMiddleName(rs.getString("middle_name"));
                nurse.setSecondName(rs.getString("last_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(statement);
            //DBUtil.close(rs);
        }

        return nurse;
    }

}
