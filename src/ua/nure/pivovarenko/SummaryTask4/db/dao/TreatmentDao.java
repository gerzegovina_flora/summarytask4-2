package ua.nure.pivovarenko.SummaryTask4.db.dao;


import ua.nure.pivovarenko.SummaryTask4.db.util.DBUtil;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Treatment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class TreatmentDao {

    private static final String APPOINT_DRUGS = "INSERT INTO \"TREATMENT\" (\"text\",discriminator, id_history, id_user, description, days) VALUES (?, 'D', ?, ?, ?, ?)";
    private static final String APPOINT_PROCEDURES = "INSERT INTO \"TREATMENT\" (discriminator, id_history, id_user, description, days,text) VALUES ('P', ?,?,?,?,?)";
    private static final String APPOINT_OPERATIONS = "INSERT INTO \"TREATMENT\" (discriminator, id_history, id_user, description, date) VALUES ('O', ?,?,?,?)";
    private static final String INSERT_COMPLAINS = "INSERT INTO \"TREATMENT\" (\"text\", discriminator, id_history, id_user, description) VALUES (?, 'C', ?,?,?)";

    public void appointDrugs(List<Treatment> treatmentList) {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            connection.setAutoCommit(false);
            for (Treatment treatment : treatmentList) {
                preparedStatement = connection.prepareStatement(APPOINT_DRUGS);
                preparedStatement.setString(1, treatment.getText());
                preparedStatement.setLong(2, treatment.getElectronicMedicalRecord().getCurrentHistory().getId());
                preparedStatement.setLong(3, treatment.getUser().getId());
                preparedStatement.setString(4, treatment.getDescription());
                preparedStatement.setString(5, treatment.getDays());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }
    }

    public void appointProcedures(List<Treatment> treatmentList) {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            connection.setAutoCommit(false);
            for (Treatment treatment : treatmentList) {
                preparedStatement = connection.prepareStatement(APPOINT_PROCEDURES);
                preparedStatement.setLong(1, treatment.getElectronicMedicalRecord().getCurrentHistory().getId());
                preparedStatement.setLong(2, treatment.getUser().getId());
                preparedStatement.setString(3, treatment.getDescription());
                preparedStatement.setString(4, treatment.getDays());
                preparedStatement.setString(5, treatment.getText());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }
    }

    public void appointOperations(List<Treatment> treatmentList) {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            connection.setAutoCommit(false);
            for (Treatment treatment : treatmentList) {
                preparedStatement = connection.prepareStatement(APPOINT_OPERATIONS);
                preparedStatement.setLong(1, treatment.getElectronicMedicalRecord().getCurrentHistory().getId());
                preparedStatement.setLong(2, treatment.getUser().getId());
                preparedStatement.setString(3, treatment.getDescription());
                preparedStatement.setString(4, treatment.getDate());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }
    }

    public void setComplains(List<Treatment> treatmentList) {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try {
            connection.setAutoCommit(false);
            for (Treatment treatment : treatmentList) {
                preparedStatement = connection.prepareStatement(INSERT_COMPLAINS);
                preparedStatement.setString(1, treatment.getText());
                preparedStatement.setLong(2, treatment.getElectronicMedicalRecord().getCurrentHistory().getId());
                preparedStatement.setLong(3, treatment.getUser().getId());
                preparedStatement.setString(4, treatment.getDescription());
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }
    }

}
