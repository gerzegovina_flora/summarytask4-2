package ua.nure.pivovarenko.SummaryTask4.db.dao;

import ua.nure.pivovarenko.SummaryTask4.db.util.DBUtil;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.*;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class PatientDao {

    private static final String SAVE_PATIENT = "INSERT INTO \"USER\"(\"name\", role, image, last_name, middle_name) VALUES (?, ?, ?, ?, ?)";
    private static final String SAVE_PATIENT_WITHOUT_IMAGE = "INSERT INTO \"USER\"(\"name\", role, last_name, middle_name) VALUES (?, ?, ?, ?)";
    private static final String GET_ALL_PATIENTS = "select * from \"PATIENT\" p , \"USER\" u, \"MEDICAL_RECORD\" mr WHERE p.id_user = u.\"id\" and p.id_medical_record = mr.id";
    private static final String GET_ALL_PATIENTS_BY_DOCTOR_ID = "select * from \"PATIENT\" p , \"HISTORY\" h, \"USER\" u WHERE u.id = p.id_user and h.id_med_rec = p.id_medical_record and h.id_doctor = ? and h.closed = false and discharged = FALSE ";
    private static final String GET_PATIENT = "select * from \"PATIENT\" p , \"USER\" u WHERE p.id_user = u.\"id\" and p.id_user = ?";
    private static final String DELETE_PATIENT = "DELETE FROM \"USER\" WHERE id = ?";
    private static final String DISCHARGE_PATIENT = "UPDATE \"PATIENT\" SET discharged = true WHERE id_user = ?";
    private static final String GET_PATIENT_FOR_EDITING = "select * from \"USER\" u, \"PATIENT\" p WHERE u.id = p.id_user and u.id = ?";
    private static final String UPDATE_USER = "UPDATE \"USER\" SET \"name\" = ?, last_name = ?, middle_name = ? WHERE id = ?;" +
            "UPDATE \"PATIENT\" SET \"work\" = ?, address = ?, phone = ? WHERE id_user = ?";

    public void update(Patient patient) {
        Connection connection = PostgresConnection.getConnection();
        try {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);
            preparedStatement.setString(1, patient.getFirstName());
            preparedStatement.setString(2, patient.getSecondName());
            preparedStatement.setString(3, patient.getMiddleName());
            preparedStatement.setLong(4, patient.getId());
            preparedStatement.setString(5, patient.getWork());
            preparedStatement.setString(6, patient.getAddress());
            preparedStatement.setString(7, patient.getPhone());
            preparedStatement.setLong(8, patient.getId());
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }
    }

    public void save(Patient patient) {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        PreparedStatement preparedStatement3 = null;
        ResultSet rs = null;
        try {
            connection.setAutoCommit(false);
            boolean image = patient.getImage().getBytesImage().length > 0;
            if (image) {
                preparedStatement1 = connection.prepareStatement(SAVE_PATIENT, PreparedStatement.RETURN_GENERATED_KEYS);
            } else {
                preparedStatement1 = connection.prepareStatement(SAVE_PATIENT_WITHOUT_IMAGE, PreparedStatement.RETURN_GENERATED_KEYS);
            }
            int index = 1;
            preparedStatement1.setString(index++, patient.getFirstName());
            preparedStatement1.setInt(index++, patient.getRole().ordinal());
            if (image)
                preparedStatement1.setBytes(index++, patient.getImage().getBytesImage());
            preparedStatement1.setString(index++, patient.getSecondName());
            preparedStatement1.setString(index, patient.getMiddleName());
            preparedStatement1.executeUpdate();
            rs = preparedStatement1.getGeneratedKeys();
            long idUser = 0;
            if (rs.next()) {
                idUser = rs.getLong(1);
            }
            preparedStatement2 = connection.prepareStatement("INSERT INTO \"MEDICAL_RECORD\" (blood_type) VALUES (?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement2.setInt(1, patient.getMedicalRecord().getBloodType().ordinal());
            preparedStatement2.executeUpdate();
            rs = preparedStatement2.getGeneratedKeys();
            long idMD = 0;
            if (rs.next()) {
                idMD = rs.getLong(1);
            }
            PreparedStatement history = connection.prepareStatement("INSERT INTO \"HISTORY\" (id_med_rec, closed, id_doctor, registry_date) VALUES (?, false,?,?)",
                    PreparedStatement.RETURN_GENERATED_KEYS);
            history.setLong(1, idMD);
            history.setLong(2, patient.getMedicalRecord().getCurrentHistory().getDoctor().getId());
            history.setDate(3, patient.getMedicalRecord().getCurrentHistory().getRegistryDate());
            history.executeUpdate();

            preparedStatement3 = connection.prepareStatement("INSERT INTO \"PATIENT\"(id_user, id_medical_record, birthday, " +
                    "gender, \"work\", address, phone) VALUES (?,?,?,?,?,?,?)");
            preparedStatement3.setLong(1, idUser);
            preparedStatement3.setLong(2, idMD);
            preparedStatement3.setDate(3, patient.getBirthday());
            preparedStatement3.setString(4, patient.getGender());
            preparedStatement3.setString(5, patient.getWork());
            preparedStatement3.setString(6, patient.getAddress());
            preparedStatement3.setString(7, patient.getPhone());
            preparedStatement3.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public void newHistoryRecord(Patient patient) {
        Connection connection = PostgresConnection.getConnection();
        try {
            connection.setAutoCommit(false);

            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO \"HISTORY\" (id_med_rec, id_doctor, registry_date) VALUES (?,?,?)");
            preparedStatement.setLong(1, patient.getMedicalRecord().getId());
            preparedStatement.setLong(2, patient.getMedicalRecord().getCurrentHistory().getDoctor().getId());
            preparedStatement.setDate(3, new Date(new java.util.Date().getTime()));
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement1 = connection.prepareStatement("UPDATE \"PATIENT\" SET discharged = false WHERE id_user = ?");
            preparedStatement1.setLong(1, patient.getId());
            preparedStatement1.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }
    }

    public List<Patient> getPatientsByDrugs() {
        Connection connection = PostgresConnection.getConnection();
        List<Patient> p = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("" +
                    "select DISTINCT(p.id_user), name from \"PATIENT\" p, \"MEDICAL_RECORD\" mr, " +
                    "\"HISTORY\" h, " +
                    "\"TREATMENT\" t, \"USER\" u WHERE mr.id = p.id_medical_record\n" +
                    "and h.id_med_rec = mr.id and t.discriminator = 'D' and t.id_history = h.id " +
                    "AND u.id = p.id_user;");
            List<Treatment> treatments = new ArrayList<>();
            while (resultSet.next()) {
                Patient patient = new Patient();
                patient.setId(resultSet.getLong("id_user"));
                patient.setFirstName(resultSet.getString("name"));

                    PreparedStatement preparedStatement = connection.prepareStatement(
                            "select * from \"PATIENT\" p, \"MEDICAL_RECORD\" mr, \"HISTORY\" h, \"TREATMENT\" t, " +
                                    "\"USER\" u WHERE mr.id = p.id_medical_record\n" +
                            "and h.id_med_rec = mr.id and t.discriminator = 'D' and t.id_history = h.id " +
                                    "AND u.id = ? and u.id = p.id_user;");
                    preparedStatement.setLong(1, patient.getId());
                    ResultSet rs = preparedStatement.executeQuery();

                    History history = new History();

                    while (rs.next()) {
                        Treatment treatment = new Treatment();
                        treatment.setDays(rs.getString("days"));
                        treatment.setText(rs.getString("text"));

                        if (treatments.size() != 0) {
                            Treatment t = treatments.get(0);
                            if (Integer.parseInt(t.getDays()) < Integer.parseInt(treatment.getDays())) {
                                treatments.remove(0);
                                treatments.add(treatment);
                            }
                        } else {
                            treatments.add(treatment);
                        }
                    }

                    history.setDrugs(treatments);

                    ElectronicMedicalRecord electronicMedicalRecord = new ElectronicMedicalRecord();
                    electronicMedicalRecord.setCurrentHistory(history);

                    patient.setMedicalRecord(electronicMedicalRecord);

                p.add(patient);
                treatments = new ArrayList<>();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return p;
    }

    public void discharge(Patient patient) {
        Connection connection = PostgresConnection.getConnection();
        try {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(DISCHARGE_PATIENT);
            preparedStatement.setLong(1, patient.getId());
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement2 = connection.prepareStatement("UPDATE \"HISTORY\" SET closed = TRUE, diagnosis = ? WHERE id = ?");
            preparedStatement2.setString(1, patient.getMedicalRecord().getCurrentHistory().getDiagnosis());
            preparedStatement2.setLong(2, patient.getMedicalRecord().getCurrentHistory().getId());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }
    }

    public void delete(Patient patient) {
        Connection connection = PostgresConnection.getConnection();
        try {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PATIENT);
            preparedStatement.setLong(1, patient.getId());
            preparedStatement.executeUpdate();
            PreparedStatement preparedStatement1 = connection.prepareStatement("DELETE FROM \"HISTORY\" h WHERE h.id_med_rec = ?");
            preparedStatement1.setLong(1, patient.getMedicalRecord().getCurrentHistory().getId());
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }
    }

    public Patient get(long id) {
        Patient patient = null;
        Connection connection = PostgresConnection.getConnection();
        try {
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(GET_PATIENT);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                patient = new Patient();
                patient.setId(resultSet.getLong("id_user"));
                patient.setDischarged(resultSet.getBoolean("discharged"));
                patient.setFirstName(resultSet.getString("name"));
                patient.getImage().setImage(resultSet.getBytes("image"));
                patient.getMedicalRecord().setId(resultSet.getLong("id_medical_record"));
                patient.setBirthday(resultSet.getDate("birthday"));
                patient.setGender(resultSet.getString("gender"));
                patient.setMiddleName(resultSet.getString("middle_name"));
                patient.setSecondName(resultSet.getString("last_name"));
                patient.setPhone(resultSet.getString("phone"));
                patient.setAddress(resultSet.getString("address"));
                patient.setWork(resultSet.getString("work"));
                patient.setImage(resultSet.getBytes("image"));
            }
            if (patient != null) {
                PreparedStatement historyStmt = connection.prepareStatement("SELECT * FROM \"HISTORY\" WHERE id_med_rec = ? and closed = true");
                historyStmt.setLong(1, patient.getMedicalRecord().getId());
                resultSet = historyStmt.executeQuery();
                while (resultSet.next()) {
                    History history = new History();
                    history.setId(resultSet.getLong("id"));
                    history.setClosed(resultSet.getBoolean("closed"));
                    history.getDoctor().setId(resultSet.getLong("id_doctor"));
                    history.setDiagnosis(resultSet.getString("diagnosis"));
                    history.setRegistryDate(resultSet.getDate("registry_date"));
                    patient.getMedicalRecord().getHistories().add(history);
                }

                for (History history : patient.getMedicalRecord().getHistories()) {
                    PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM \"USER\" WHERE id = ?");
                    preparedStatement1.setLong(1, history.getDoctor().getId());
                    resultSet = preparedStatement1.executeQuery();
                    while (resultSet.next()) {
                        history.getDoctor().setFirstName(resultSet.getString("name"));
                    }

                    PreparedStatement treatmentDrugs = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'D'");
                    treatmentDrugs.setLong(1, history.getId());
                    resultSet = treatmentDrugs.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDays(resultSet.getString("days"));
                        treatment.setText(resultSet.getString("text"));
                        treatment.setUser(user);
                        history.getDrugs().add(treatment);
                    }

                    for (Treatment treatment : history.getDrugs()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentComplain = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'C'");
                    treatmentComplain.setLong(1, history.getId());
                    resultSet = treatmentComplain.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setUser(user);
                        history.getComplains().add(treatment);
                    }

                    for (Treatment treatment : history.getComplains()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentOperations = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'O'");
                    treatmentOperations.setLong(1, history.getId());
                    resultSet = treatmentOperations.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDate(resultSet.getString("date"));
                        treatment.setUser(user);
                        history.getOperations().add(treatment);
                    }

                    for (Treatment treatment : history.getOperations()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentProcedures = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'P'");
                    treatmentProcedures.setLong(1, history.getId());
                    resultSet = treatmentProcedures.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDays(resultSet.getString("days"));
                        treatment.setText(resultSet.getString("text"));
                        treatment.setUser(user);
                        history.getProcedures().add(treatment);
                    }
                    for (Treatment treatment : history.getProcedures()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }
                }

                // GET CURRENT HISTORY
                PreparedStatement activeStory = connection.prepareStatement("SELECT * FROM \"HISTORY\" WHERE id_med_rec = ? and closed = FALSE");
                activeStory.setLong(1, patient.getMedicalRecord().getId());
                resultSet = activeStory.executeQuery();
                while (resultSet.next()) {
                    patient.getMedicalRecord().getCurrentHistory().setId(resultSet.getLong("id"));
                    patient.getMedicalRecord().getCurrentHistory().setClosed(resultSet.getBoolean("closed"));
                    patient.getMedicalRecord().getCurrentHistory().setRegistryDate(resultSet.getDate("registry_date"));
                    patient.getMedicalRecord().getCurrentHistory().getDoctor().setId(resultSet.getLong("id_doctor"));
                }

                if (!patient.getDischarged()) {
                    PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM \"USER\" WHERE id = ?");
                    preparedStatement1.setLong(1, patient.getMedicalRecord().getCurrentHistory().getDoctor().getId());
                    resultSet = preparedStatement1.executeQuery();
                    while (resultSet.next()) {
                        patient.getMedicalRecord().getCurrentHistory().getDoctor().setFirstName(resultSet.getString("name"));
                    }

                    PreparedStatement treatmentDrugs = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'D'");
                    treatmentDrugs.setLong(1, patient.getMedicalRecord().getCurrentHistory().getId());
                    resultSet = treatmentDrugs.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDays(resultSet.getString("days"));
                        treatment.setText(resultSet.getString("text"));
                        treatment.setUser(user);
                        patient.getMedicalRecord().getCurrentHistory().getDrugs().add(treatment);
                    }

                    for (Treatment treatment : patient.getMedicalRecord().getCurrentHistory().getDrugs()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentComplains = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'C'");
                    treatmentComplains.setLong(1, patient.getMedicalRecord().getCurrentHistory().getId());
                    resultSet = treatmentComplains.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDays(resultSet.getString("days"));
                        treatment.setText(resultSet.getString("text"));
                        treatment.setUser(user);
                        patient.getMedicalRecord().getCurrentHistory().getComplains().add(treatment);
                    }

                    for (Treatment treatment : patient.getMedicalRecord().getCurrentHistory().getComplains()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentOperations = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'O'");
                    treatmentOperations.setLong(1, patient.getMedicalRecord().getCurrentHistory().getId());
                    resultSet = treatmentOperations.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDate(resultSet.getString("date"));
                        treatment.setUser(user);
                        patient.getMedicalRecord().getCurrentHistory().getOperations().add(treatment);
                    }

                    for (Treatment treatment : patient.getMedicalRecord().getCurrentHistory().getOperations()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }

                    PreparedStatement treatmentProcedures = connection.prepareStatement("SELECT * FROM \"TREATMENT\" WHERE id_history = ? and discriminator = 'P'");
                    treatmentProcedures.setLong(1, patient.getMedicalRecord().getCurrentHistory().getId());
                    resultSet = treatmentProcedures.executeQuery();
                    while (resultSet.next()) {
                        User user = new User();
                        user.setId(resultSet.getLong("id_user"));
                        Treatment treatment = new Treatment();
                        treatment.setId(resultSet.getLong("id"));
                        treatment.setDescription(resultSet.getString("description"));
                        treatment.setDays(resultSet.getString("days"));
                        treatment.setText(resultSet.getString("text"));
                        treatment.setUser(user);
                        patient.getMedicalRecord().getCurrentHistory().getProcedures().add(treatment);
                    }
                    for (Treatment treatment : patient.getMedicalRecord().getCurrentHistory().getProcedures()) {
                        PreparedStatement doctor = connection.prepareStatement("SELECT * FROM \"USER\" WHERE \"id\" = ?");
                        doctor.setLong(1, treatment.getUser().getId());
                        resultSet = doctor.executeQuery();
                        while (resultSet.next()) {
                            User user = new User();
                            user.setFirstName(resultSet.getString("name"));
                            treatment.setUser(user);
                        }
                    }
                }
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }

        return patient;
    }

    public Patient getPatientForEdit(Long id) {
        Patient patient = null;
        Connection connection = PostgresConnection.getConnection();
        ResultSet rs = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_PATIENT_FOR_EDITING);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                patient = new Patient();
                patient.setId(rs.getLong("id_user"));
                patient.setFirstName(rs.getString("name"));
                patient.setMiddleName(rs.getString("middle_name"));
                patient.setSecondName(rs.getString("last_name"));
                patient.setPhone(rs.getString("phone"));
                patient.setWork(rs.getString("work"));
                patient.setAddress(rs.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(statement);
            //DBUtil.close(rs);
        }

        return patient;
    }

    public List<Patient> getPatientListByDoctorId(long id) {
        List<Patient> patientList = null;
        Connection connection = PostgresConnection.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_PATIENTS_BY_DOCTOR_ID);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            patientList = new ArrayList<>();
            while (resultSet.next()) {
                Patient patient = new Patient();
                patient.setId(resultSet.getLong("id_user"));
                patient.setFirstName(resultSet.getString("name"));
                patient.getImage().setImage(resultSet.getBytes("image"));
                patient.setBirthday(resultSet.getDate("birthday"));
                patientList.add(patient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }

        return patientList;
    }

    public List<Patient> getPatientList() {
        List<Patient> patientList = null;
        Connection connection = PostgresConnection.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_ALL_PATIENTS);
            patientList = new ArrayList<>();
            while (resultSet.next()) {
                Patient patient = new Patient();
                patient.setId(resultSet.getLong("id_user"));
                patient.setFirstName(resultSet.getString("name"));
                patient.getImage().setImage(resultSet.getBytes("image"));
                patient.setBirthday(resultSet.getDate("birthday"));
                patient.getMedicalRecord().getCurrentHistory().setId(resultSet.getLong("id_medical_record"));
                patientList.add(patient);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }

        return patientList;
    }

}
