package ua.nure.pivovarenko.SummaryTask4.db.dao;

import ua.nure.pivovarenko.SummaryTask4.db.util.DBUtil;
import ua.nure.pivovarenko.SummaryTask4.domain.DoctorType;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DoctorDao {

    private static final String GET_ALL_DOCTORS = "select * from \"DOCTOR\" d , \"USER\" u , \"DOCTOR_TYPE\" dt WHERE d.id_user = u.\"id\" and d.\"id_type\" = dt.\"id\"";
    private static final String GET_DOCTOR = "select * from \"DOCTOR\" d , \"USER\" u , \"DOCTOR_TYPE\" dt WHERE d.id_user = u.\"id\" and d.\"id_type\" = dt.\"id\" and u.id = ?";
    private static final String GET_ALL_DOCTORS_BY_TYPE = "select * from \"USER\" u left outer join \"DOCTOR\" d on d.id_user = u.\"id\", \"DOCTOR_TYPE\" dt WHERE dt.\"id\" = ?";
    private static final String GET_ALL_DOCTOR_TYPES = "select * from \"DOCTOR_TYPE\"";
    private static final String SAVE_DOCTOR = "INSERT INTO \"USER\"(login, \"name\", role, password, image, last_name, middle_name) VALUES (?, ?, ?, md5(?), [?],?,?)";
    private static final String UPDATE_DOCTOR = "UPDATE \"USER\" SET login = ?, \"name\" = ?, password = md5(?), last_name = ?, middle_name = ? WHERE id = ?";
    private static final String UPDATE_DOCTOR_WITHOUT_PASSWORD = "UPDATE \"USER\" SET login = ?, \"name\" = ?, last_name = ?, middle_name = ? WHERE id = ?";
    private static final String DELETE_DOCTOR = "DELETE FROM \"USER\" WHERE id = ?";

    public Doctor get(Long id){
        Doctor doctor = null;
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try{
            preparedStatement = connection.prepareStatement(GET_DOCTOR);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            while (rs.next()){
                doctor = new Doctor();
                doctor.setId(rs.getLong("id_user"));
                doctor.setFirstName(rs.getString("name"));
                doctor.setLogin(rs.getString("login"));
                doctor.setPassword(rs.getString("password"));
                doctor.getImage().setImage(rs.getBytes("image"));
                doctor.setType(DoctorType.valueOf(rs.getString("type")));
                doctor.setSecondName(rs.getString("last_name"));
                doctor.setMiddleName(rs.getString("middle_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }

        return doctor;
    }

    public void delete(long id){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        try{
            connection.setAutoCommit(false);
            preparedStatement1 = connection.prepareStatement(DELETE_DOCTOR);
            preparedStatement1.setLong(1, id);
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public List<Doctor> getDoctorsByType(Long id){
        List<Doctor> doctorList = null;
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try{
            preparedStatement = connection.prepareStatement(GET_ALL_DOCTORS_BY_TYPE);
            preparedStatement.setLong(1, id);
            rs = preparedStatement.executeQuery();
            doctorList = new ArrayList<>();
            while (rs.next()){
                Doctor doctor = new Doctor();
                doctor.setId(rs.getLong("id_user"));
                doctor.setFirstName(rs.getString("name"));
                doctor.setLogin(rs.getString("login"));
                doctor.setPassword(rs.getString("password"));
                doctor.getImage().setImage(rs.getBytes("image"));
                doctor.setType(DoctorType.valueOf(rs.getString("type")));
                doctorList.add(doctor);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }

        return doctorList;
    }

    public List<Doctor> getDoctorList(){
        List<Doctor> doctorList = null;
        Connection connection = PostgresConnection.getConnection();
        Statement statement = null;
        ResultSet rs = null;
        try{
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_DOCTORS);
            doctorList = new ArrayList<>();
            while (rs.next()){
                Doctor doctor = new Doctor();
                doctor.setId(rs.getLong("id_user"));
                doctor.setFirstName(rs.getString("name"));
                doctor.setLogin(rs.getString("login"));
                doctor.setPassword(rs.getString("password"));
                doctor.getImage().setImage(rs.getBytes("image"));
                doctor.setType(DoctorType.valueOf(rs.getString("type")));
                doctor.setMiddleName(rs.getString("middle_name"));
                doctor.setSecondName(rs.getString("last_name"));
                doctorList.add(doctor);
            }

            for(Doctor doctor : doctorList){
                PreparedStatement preparedStatement1 = connection.prepareStatement("select * FROM \"HISTORY\" where id_doctor = ? and closed = false");
                preparedStatement1.setLong(1, doctor.getId());
                ResultSet resultSet = preparedStatement1.executeQuery();
                while (resultSet.next()){
                    doctor.getPatientList().add(new Patient());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(statement);
            //DBUtil.close(rs);
        }

        return doctorList;
    }

    public List<Doctor> getDoctorTypes(){
        List<Doctor> doctorList = null;
        Connection connection = PostgresConnection.getConnection();
        Statement statement = null;
        ResultSet rs = null;
        try{
            statement = connection.createStatement();
            rs = statement.executeQuery(GET_ALL_DOCTOR_TYPES);
            doctorList = new ArrayList<>();
            while (rs.next()){
                Doctor doctor = new Doctor();
                doctor.setId(rs.getLong("id_user"));
                doctor.setType(DoctorType.valueOf(rs.getString("type")));
                doctorList.add(doctor);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }finally {
            //DBUtil.close(connection);
            //DBUtil.close(statement);
            //DBUtil.close(rs);
        }

        return doctorList;
    }

    public void save(Doctor doctor){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        ResultSet rs = null;
        try{
            connection.setAutoCommit(false);
            boolean isImage = doctor.getImage().getBytesImage().length > 0;
            String save = SAVE_DOCTOR;
            if(isImage){
                save = save.replace("[?]", "?");
            }else{
                save = save.replace("[?]", "default");
            }
            int index = 1;
            preparedStatement1 = connection.prepareStatement(save, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement1.setString(index++, doctor.getLogin());
            preparedStatement1.setString(index++, doctor.getFirstName());
            preparedStatement1.setInt(index++, doctor.getRole().ordinal());
            preparedStatement1.setString(index++, doctor.getPassword());
            if(isImage){
                preparedStatement1.setBytes(index++, doctor.getImage().getBytesImage());
            }
            preparedStatement1.setString(index++, doctor.getSecondName());
            preparedStatement1.setString(index, doctor.getMiddleName());
            preparedStatement1.executeUpdate();
            rs = preparedStatement1.getGeneratedKeys();
            long idUser = 0;
            if (rs.next()) {
                idUser = rs.getLong(1);
            }
            preparedStatement2 = connection.prepareStatement("INSERT INTO \"DOCTOR\"(id_user, id_type) VALUES (?,?)");
            preparedStatement2.setLong(1, idUser);
            preparedStatement2.setInt(2, doctor.getType().ordinal());
            preparedStatement2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }

    public void update(Doctor doctor){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement1 = null;
        try{
            connection.setAutoCommit(false);
            boolean password = !Objects.equals(doctor.getPassword(), "");
            if(password)
                preparedStatement1 = connection.prepareStatement(UPDATE_DOCTOR);
            else
                preparedStatement1 = connection.prepareStatement(UPDATE_DOCTOR_WITHOUT_PASSWORD);
            int index = 1;
            preparedStatement1.setString(index++, doctor.getLogin());
            preparedStatement1.setString(index++, doctor.getFirstName());
            if(password){
                preparedStatement1.setString(index++, doctor.getPassword());
            }
            preparedStatement1.setString(index++, doctor.getSecondName());
            preparedStatement1.setString(index++, doctor.getMiddleName());
            preparedStatement1.setLong(index++, doctor.getId());
            preparedStatement1.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        } finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement1);
            //DBUtil.close(preparedStatement2);
            //DBUtil.close(rs);
        }
    }
}
