package ua.nure.pivovarenko.SummaryTask4.db.dao;


import ua.nure.pivovarenko.SummaryTask4.db.util.DBUtil;
import ua.nure.pivovarenko.SummaryTask4.domain.Role;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {

    private static final String GET_USER = "SELECT * FROM \"USER\" WHERE \"login\" = ? and \"password\" = md5(?)";
    private static final String GET_USER_LOGIN = "SELECT * FROM \"USER\" WHERE \"login\" = ?";
    private static final String UPDATE_USER = "UPDATE \"USER\" SET login = ?, \"name\" = ?, last_name = ?, middle_name = ?, password = md5(?) WHERE id = ?";
    private static final String UPDATE_USER_WITHOUT_PASSWORD = "UPDATE \"USER\" SET login = ?, \"name\" = ?, last_name = ?, middle_name = ? WHERE id = ?";

    public User get(User user) throws DAOException {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        User u = null;
        try{
            preparedStatement = connection.prepareStatement(GET_USER);
            preparedStatement.setString(1 ,user.getLogin());
            preparedStatement.setString(2 ,user.getPassword());
            rs = preparedStatement.executeQuery();
            while (rs.next()){
                u = new User();
                u.setMiddleName(rs.getString("middle_name"));
                u.setSecondName(rs.getString("last_name"));
                u.setId(rs.getLong("id"));
                u.setFirstName(rs.getString("name"));
                u.setLogin(rs.getString("login"));
                u.setRole(Role.values()[rs.getInt("role")]);
                u.setPassword(rs.getString("password"));
                u.getImage().setImage(rs.getBytes("image"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
            throw new DAOException("User not found", e);
        }finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }

        return u;
    }

    public User getByLogin(User user) throws DAOException {
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        User u = null;
        try{
            preparedStatement = connection.prepareStatement(GET_USER_LOGIN);
            preparedStatement.setString(1 ,user.getLogin());
            rs = preparedStatement.executeQuery();
            while (rs.next()){
                u = new User();
                u.setMiddleName(rs.getString("middle_name"));
                u.setSecondName(rs.getString("last_name"));
                u.setId(rs.getLong("id"));
                u.setFirstName(rs.getString("name"));
                u.setLogin(rs.getString("login"));
                u.setRole(Role.values()[rs.getInt("role")]);
                u.setPassword(rs.getString("password"));
                u.getImage().setImage(rs.getBytes("image"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
            throw new DAOException("User not found", e);
        }finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }

        return u;
    }

    public void update(User user){
        Connection connection = PostgresConnection.getConnection();
        PreparedStatement preparedStatement = null;
        try{
            int index = 1;
            boolean updatePassword = !user.getPassword().equals("");
            if(updatePassword){
                preparedStatement = connection.prepareStatement(UPDATE_USER);
            }else{
                preparedStatement = connection.prepareStatement(UPDATE_USER_WITHOUT_PASSWORD);
            }
            preparedStatement.setString(index++ ,user.getLogin());
            preparedStatement.setString(index++ ,user.getFirstName());
            preparedStatement.setString(index++, user.getSecondName());
            preparedStatement.setString(index++, user.getMiddleName());
            if(updatePassword){
                preparedStatement.setString(index++, user.getPassword());
            }
            preparedStatement.setLong(index, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            DBUtil.rollback(connection);
        }finally {
            //DBUtil.close(connection);
            //DBUtil.close(preparedStatement);
            //DBUtil.close(rs);
        }
    }

}
