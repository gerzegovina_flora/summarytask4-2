<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="select" uri="http://select.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://security.pivovarenko.nure.ua" %>

<security:Security hasRole="ADMIN"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="content.patients.add"/> </title>

    <!-- Bootstrap core CSS -->

    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/pages/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/pages/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/icheck/flat/green.css" rel="stylesheet">
    <!-- editor -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/editor/index.css" rel="stylesheet">
    <!-- select2 -->
    <link href="${pageContext.request.contextPath}/pages/css/select/select2.min.css" rel="stylesheet">

    <!-- switchery -->
    <link rel="stylesheet" href="css/switchery/switchery.min.css" />

    <script src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/script.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body class="nav-md" onload="getAllDoctors('${pageContext.request.contextPath}')">
    <div class="container body">
        <div class="main_container">
            <%@ include file="fragments/left-bar.jspf" %>
            <%@ include file="fragments/top-navigator.jspf" %>
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="page-title">

                    </div>
                    <div class="clearfix"></div>
                    <form class="form-horizontal form-label-left" method="post" action="${pageContext.request.contextPath}/add-patient.htm" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><fmt:message key="content.patients.basicinformation"/> </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix">

                                    </div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <!-- MAIN FORM -->
                                    <c:set var="error_fName" value="${requestScope.errors.errors['error.user.name.first']}"/>
                                    <div class="item form-group ${ error_fName.result? 'bad' : ''}">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <fmt:message key="content.name.first"/> <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" id="first-name" value="${error_fName.value}" pattern="^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}" required="required" class="form-control col-md-7 col-xs-12" name="fName">
                                        </div>
                                        <c:if test="${ error_fName.result}">
                                            <div class="alert"><fmt:message key="error.user.name.first"/></div>
                                        </c:if>
                                    </div>
                                    <c:set var="error_sName" value="${requestScope.errors.errors['error.user.name.second']}"/>
                                    <div class="item form-group ${ error_sName.result? 'bad' : ''}">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> <fmt:message key="content.name.last"/> <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="second-name" value="${error_sName.value}" required="required" pattern="^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}" class="form-control col-md-7 col-xs-12" type="text" name="sName">
                                        </div>
                                        <c:if test="${ error_sName.result}">
                                            <div class="alert"><fmt:message key="error.user.name.second"/></div>
                                        </c:if>
                                    </div>
                                    <c:set var="error_mName" value="${requestScope.errors.errors['error.user.name.middle']}"/>
                                    <div class="item form-group ${ error_mName.result? 'bad' : ''}">
                                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12"> <fmt:message key="content.name.middle"/> <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="middle-name" value="${error_mName.value}" required="required" pattern="^(?![ЪъЬьЫы^!-/:-@])[A-ZА-ЯЇҐІ][ \-'0-9A-Za-zА-ЯЪІҐЇЁа-яїґъіё]{1,35}" class="form-control col-md-7 col-xs-12" type="text" name="mName">
                                        </div>
                                        <c:if test="${ error_mName.result}">
                                            <div class="alert"><fmt:message key="error.user.name.middle"/></div>
                                        </c:if>
                                    </div>
                                    <c:set var="error_gender" value="${requestScope.errors.errors['error.user.gender']}"/>
                                        <div class="item form-group ${ error_gender.result? 'bad' : ''}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> <fmt:message key="content.treatment.gender"/> <span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div id="gender" class="btn-group" data-toggle="buttons">
                                                    <fmt:message key="content.treatment.gender.m"/>: <input type="radio" class="flat" name="gender" id="genderM" value="M" checked />
                                                    <fmt:message key="content.treatment.gender.f"/>: <input type="radio" class="flat" name="gender" id="genderF" value="F" />
                                                </div>
                                            </div>
                                            <c:if test="${ error_gender.result}">
                                                <div class="alert"><fmt:message key="error.user.gender"/></div>
                                            </c:if>
                                        </div>
                                        <c:set var="error_date" value="${requestScope.errors.errors['error.user.date']}"/>
                                        <div class="item form-group ${ error_date.result? 'bad' : ''}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> <fmt:message key="content.data.birthday"/> <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="birthday" value="${error_date.value}" name="birthday" class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" />
                                            </div>
                                            <c:if test="${ error_date.result}">
                                                <div class="alert"><fmt:message key="error.user.date"/></div>
                                            </c:if>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"> <fmt:message key="content.photo"/> <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input id="photo" class="date-picker form-control col-md-7 col-xs-12" type="file" name="photo" accept="image/jpeg">
                                            </div>
                                        </div>
                                        <input type="hidden" name="command" value="add_patient"/>
                                        <div class="ln_solid"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#birthday').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                            $('#date-registry').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_4"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

                    <div class="row">

                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><fmt:message key="content.card"/></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                            <div class="item form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.blood"/><span class="required">*</span></label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select:SelectList classUrl="ua.nure.pivovarenko.SummaryTask4.domain.BloodType" name="blood" localePrefix="content.blood"/>
                                                </div>
                                            </div>
                                    <c:set var="error_doctor" value="${requestScope.errors.errors['error.user.treatment.doctor']}"/>
                                        <div class="item form-group ${error_doctor.result ? 'bad' : ''}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.treatment.doctor"/><span class="required">*</span></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="select2_single form-control" tabindex="-1" name="doctor" id="patDoc" required="required">
                                                    <option value="-1"></option>
                                                </select>
                                            </div>
                                            <c:if test="${error_doctor.result}">
                                                <div class="alert"><fmt:message key="error.user.treatment.doctor"/></div>
                                            </c:if>
                                        </div>
                                    <c:set var="error_date_registry" value="${requestScope.errors.errors['error.user.date.registry']}"/>
                                    <div class="item form-group ${ error_date_registry.result? 'bad' : ''}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.treatment.date"/><span class="required">*</span></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input id="date-registry" type="text" value="${error_date_registry.value}" class="form-control col-md-7 col-xs-12" name="registryDate"/>
                                            </div>
                                        <c:if test="${ error_date_registry.result}">
                                            <div class="alert"><fmt:message key="error.user.date.registry"/></div>
                                        </c:if>
                                        </div>
                                        <div class="ln_solid"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><fmt:message key="content.patients.additionalinformation"/></h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br />
                                    <c:set var="error_phone" value="${requestScope.errors.errors['error.user.phone']}"/>
                                        <div class="item form-group ${ error_gender.result? 'bad' : ''}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-3"><fmt:message key="content.treatment.telephone"/></label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <input type="text" value="${error_phone.value}" class="form-control" name="phone">
                                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                            <c:if test="${ error_phone.result}">
                                                <div class="alert"><fmt:message key="error.user.phone"/></div>
                                            </c:if>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.treatment.work"/></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12" name="work"/>
                                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.treatment.address"/></label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" class="form-control col-md-7 col-xs-12" name="address"/>
                                                <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                            </div>

                                            <button type="submit" class="btn btn-success"><fmt:message key="content.add"/></button>
                                        </div>
                                    <div class="ln_solid">

                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
                <%@ include file="fragments/footer.jspf" %>
            </div>
        </div>
    </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>

        <script src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>

        <!-- chart js -->
        <script src="${pageContext.request.contextPath}/pages/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="${pageContext.request.contextPath}/pages/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="${pageContext.request.contextPath}/pages/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="${pageContext.request.contextPath}/pages/js/icheck/icheck.min.js"></script>
        <!-- tags -->
        <script src="${pageContext.request.contextPath}/pages/js/tags/jquery.tagsinput.min.js"></script>
        <!-- switchery -->
        <script src="${pageContext.request.contextPath}/pages/js/switchery/switchery.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/moment.min2.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/datepicker/daterangepicker.js"></script>
        <!-- richtext editor -->
        <script src="${pageContext.request.contextPath}/pages/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="${pageContext.request.contextPath}/pages/js/editor/external/jquery.hotkeys.js"></script>
        <script src="${pageContext.request.contextPath}/pages/js/editor/external/google-code-prettify/prettify.js"></script>
        <!-- select2 -->
        <script src="${pageContext.request.contextPath}/pages/js/select/select2.full.js"></script>
        <!-- form validation -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/parsley/parsley.min.js"></script>
        <!-- textarea resize -->
        <script src="${pageContext.request.contextPath}/pages/js/textarea/autosize.min.js"></script>
        <script>
            autosize($('.resizable_textarea'));
        </script>
        <!-- Autocomplete -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/autocomplete/countries.js"></script>
        <script src="${pageContext.request.contextPath}/pages/js/autocomplete/jquery.autocomplete.js"></script>
        <script type="text/javascript">
            $(function () {
                'use strict';
                var countriesArray = $.map(countries, function (value, key) {
                    return {
                        value: value,
                        data: key
                    };
                });
                // Initialize autocomplete with custom appendTo:
                $('#autocomplete-custom-append').autocomplete({
                    lookup: countriesArray,
                    appendTo: '#autocomplete-container'
                });
            });
        </script>
        <script src="${pageContext.request.contextPath}/pages/js/custom.js"></script>


        <!-- select2 -->
        <script>
            $(document).ready(function () {
                $(".select2_single").select2({
                    placeholder: "Select a state",
                    allowClear: true
                });
                $(".select2_group").select2({});
                $(".select2_multiple").select2({
                    maximumSelectionLength: 4,
                    placeholder: "With Max Selection limit 4",
                    allowClear: true
                });
            });
        </script>
        <!-- /select2 -->
        <!-- input tags -->
        <script>
            function onAddTag(tag) {
                alert("Added a tag: " + tag);
            }

            function onRemoveTag(tag) {
                alert("Removed a tag: " + tag);
            }

            function onChangeTag(input, tag) {
                alert("Changed a tag: " + tag);
            }

            $(function () {
                $('#tags_1').tagsInput({
                    width: 'auto'
                });
            });
        </script>
        <!-- /input tags -->
        <!-- form validation -->
        <script type="text/javascript">
            $(document).ready(function () {
                $.listen('parsley:field:validate', function () {
                    validateFront();
                });
                $('#demo-form .btn').on('click', function () {
                    $('#demo-form').parsley().validate();
                    validateFront();
                });
                var validateFront = function () {
                    if (true === $('#demo-form').parsley().isValid()) {
                        $('.bs-callout-info').removeClass('hidden');
                        $('.bs-callout-warning').addClass('hidden');
                    } else {
                        $('.bs-callout-info').addClass('hidden');
                        $('.bs-callout-warning').removeClass('hidden');
                    }
                };
            });

            $(document).ready(function () {
                $.listen('parsley:field:validate', function () {
                    validateFront();
                });
                $('#demo-form2 .btn').on('click', function () {
                    $('#demo-form2').parsley().validate();
                    validateFront();
                });
                var validateFront = function () {
                    if (true === $('#demo-form2').parsley().isValid()) {
                        $('.bs-callout-info').removeClass('hidden');
                        $('.bs-callout-warning').addClass('hidden');
                    } else {
                        $('.bs-callout-info').addClass('hidden');
                        $('.bs-callout-warning').removeClass('hidden');
                    }
                };
            });
            try {
                hljs.initHighlightingOnLoad();
            } catch (err) {}
        </script>
        <!-- /form validation -->
        <!-- editor -->
        <script>
            $(document).ready(function () {
                $('.xcxc').click(function () {
                    $('#descr').val($('#editor').html());
                });
            });

            $(function () {
                function initToolbarBootstrapBindings() {
                    var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'],
                        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                    $.each(fonts, function (idx, fontName) {
                        fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                    });
                    $('a[title]').tooltip({
                        container: 'body'
                    });
                    $('.dropdown-menu input').click(function () {
                            return false;
                        })
                        .change(function () {
                            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                        })
                        .keydown('esc', function () {
                            this.value = '';
                            $(this).change();
                        });

                    $('[data-role=magic-overlay]').each(function () {
                        var overlay = $(this),
                            target = $(overlay.data('target'));
                        overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                    });
                    if ("onwebkitspeechchange" in document.createElement("input")) {
                        var editorOffset = $('#editor').offset();
                        $('#voiceBtn').css('position', 'absolute').offset({
                            top: editorOffset.top,
                            left: editorOffset.left + $('#editor').innerWidth() - 35
                        });
                    } else {
                        $('#voiceBtn').hide();
                    }
                };

                function showErrorAlert(reason, detail) {
                    var msg = '';
                    if (reason === 'unsupported-file-type') {
                        msg = "Unsupported format " + detail;
                    } else {
                        console.log("error uploading file", reason, detail);
                    }
                    $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                        '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                };
                initToolbarBootstrapBindings();
                $('#editor').wysiwyg({
                    fileUploadError: showErrorAlert
                });
                window.prettyPrint && prettyPrint();
            });
        </script>
        <!-- /editor -->
</body>

</html>