<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 04.03.2016
  Time: 10:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
<c:forEach var="patient" items="${sessionScope.mostDrugs}">
  ${patient.id} ${patient.firstName}
  <c:forEach var="drug" items="${patient.medicalRecord.currentHistory.drugs}">
    ${drug.days} ${drug.text}
  </c:forEach>
  <br/> <br/>
  </c:forEach>
</body>
</html>
