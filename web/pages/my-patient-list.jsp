<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="select" uri="http://select.pivovarenko.nure.ua" %>
<%@ taglib prefix="security" uri="http://security.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:Security hasRole="DOCTOR"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="content.patients.my"/> </title>

    <!-- Bootstrap core CSS -->

    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/pages/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/pages/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/icheck/flat/green.css" rel="stylesheet">


    <script src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <%@ include file="fragments/left-bar.jspf" %>
            <%@ include file="fragments/top-navigator.jspf" %>
            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><fmt:message key="content.patients"/></h3>
                        </div>

                        <div class="title_right">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="row">
                                        <div class="clearfix"></div>
                                        <c:forEach var="patient" items="${requestScope.patientList}">
                                        <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <h4 class="brief"><i><fmt:message key="content.patients"/></i></h4>
                                                    <div class="left col-xs-7">
                                                        <h2>${patient.firstName} ${patient.secondName}</h2>
                                                        <p><strong><fmt:message key="content.data.birthday"/>: </strong> ${patient.birthday} </p><br/><br/>
                                                    </div>
                                                    <div class="right col-xs-5 text-center">
                                                        <img src="${patient.image.imageBase64}" alt="" class="img-circle img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-12 emphasis">
                                                            <a href="${pageContext.request.contextPath}/view-patient.htm?command=patient_info&id=${patient.id}"><button type="button" class="btn btn-primary btn-xs"> <i class="fa fa-user">
                                                            </i> <fmt:message key="content.view"/> </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <%@ include file="fragments/footer.jspf" %>
                </div>
            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="${pageContext.request.contextPath}/pages/js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="${pageContext.request.contextPath}/pages/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="${pageContext.request.contextPath}/pages/js/icheck/icheck.min.js"></script>

    <script src="${pageContext.request.contextPath}/pages/js/custom.js"></script>

</body>

</html>