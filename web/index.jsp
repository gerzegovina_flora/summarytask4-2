<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
  <title>Singin</title>
  <meta charset="utf-8">

  <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="${pageContext.request.contextPath}/pages/css/custom.css" rel="stylesheet">

<%--  <script src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>--%>
  <link href="${pageContext.request.contextPath}/pages/css/style.css" rel='stylesheet' type='text/css' />
</head>
<body>

<div class="main">
  <div class="login-form">
    <h1>Welcome</h1>
    <div class="head">
      <img src="${pageContext.request.contextPath}/pages/images/user2.png" alt=""/>
    </div>
    <form method="post" action="${pageContext.request.contextPath}/login.htm">
      <input type="text" class="text" placeholder="administrator" name="login" required/>
      <input type="password" placeholder="1111" name="password" required />
      <input type="hidden" name="command" value="login">
      <div class="form-group">
        </div>
      <div class="submit">
        <input type="submit" value="<fmt:message key="content.singin"/>" >
      </div>
      <c:if test="${requestScope.sessionEnd}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
          <fmt:message key="error.session.end"/>
        </div>
      </c:if>
      <c:if test="${param.error eq 'auth'}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
          <fmt:message key="error.user.found"/>
        </div>
      </c:if>
    </form>
  </div>
</div>

</body>
</html>