import dao.AllDaoTest;
import entity.AllEntityTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AllEntityTest.class,
        AllDaoTest.class
})
public class AllTests {
}
