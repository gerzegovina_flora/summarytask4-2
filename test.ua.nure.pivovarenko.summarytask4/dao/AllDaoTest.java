package dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        NurseDaoTest.class
})
public class AllDaoTest {
}
