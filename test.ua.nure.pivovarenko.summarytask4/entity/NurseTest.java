package entity;

import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.domain.Role;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Nurse;

public class NurseTest {

    private Nurse nurse = new Nurse();

    @Test
    public void testNurseConstructor(){
        nurse = new Nurse();
    }

    @Test
    public void testNurseSetters(){
        nurse.setSecondName("sName");
        nurse.setRole(Role.NURSE);
        nurse.setId(0L);
        nurse.setPassword("123");
        nurse.setLogin("login");
        nurse.setMiddleName("mName");
        nurse.setFirstName("fName");
        nurse.setImage(new byte[0]);
    }

    @Test
    public void testNurseGetters(){
        nurse.getFirstName();
        nurse.getId();
        nurse.getSecondName();
        nurse.getImage();
        nurse.getRole();
        nurse.getMiddleName();
        nurse.getPassword();
        nurse.getImage();
    }

}
