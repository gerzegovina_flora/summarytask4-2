package entity;


import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.domain.BloodType;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.ElectronicMedicalRecord;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.History;

import java.util.ArrayList;

public class ElectronicMedicalRecordTest {

    private ElectronicMedicalRecord electronicMedicalRecord = new ElectronicMedicalRecord();

    @Test
    public void testElectronicMedicalRecordConstructor(){
        electronicMedicalRecord = new ElectronicMedicalRecord();
    }

    @Test
    public void testElectronicMedicalRecordSetters(){
        electronicMedicalRecord.setId(0L);
        electronicMedicalRecord.setBloodType(BloodType.A_P);
        electronicMedicalRecord.setCurrentHistory(new History());
        electronicMedicalRecord.setHistories(new ArrayList<History>());
    }

    @Test
    public void testElectronicMedicalRecordGetters(){
        electronicMedicalRecord.getId();
        electronicMedicalRecord.getBloodType();
        electronicMedicalRecord.getCurrentHistory();
        electronicMedicalRecord.getHistories();
    }
}
