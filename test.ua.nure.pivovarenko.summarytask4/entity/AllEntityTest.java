package entity;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DoctorTest.class,
        ElectronicMedicalRecordTest.class,
        HistoryTest.class,
        NurseTest.class
})
public class AllEntityTest {
}
