package entity;

import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.domain.DoctorType;
import ua.nure.pivovarenko.SummaryTask4.domain.Role;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;

public class DoctorTest {

    private Doctor doctor = new Doctor();

    @Test
    public void testDoctorConstructor(){
        doctor = new Doctor();
    }

    @Test
    public void testDoctorSetters(){
        doctor.setImage(new byte[0]);
        doctor.setId(0L);
        doctor.setType(DoctorType.ALLERGIST);
        doctor.setFirstName("John");
        doctor.setMiddleName("J");
        doctor.setLogin("john");
        doctor.setPassword("123");
        doctor.setRole(Role.DOCTOR);
        doctor.setSecondName("Doe");
    }

    @Test
    public void testDoctorGetters(){
        doctor.getImage();
        doctor.getPatientList();
        doctor.getPassword();
        doctor.getType();
        doctor.getLogin();
        doctor.getId();
        doctor.getFirstName();
        doctor.getSecondName();
        doctor.getMiddleName();
    }
}
