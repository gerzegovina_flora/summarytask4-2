package entity;

import org.junit.Test;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Doctor;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.History;
import ua.nure.pivovarenko.SummaryTask4.domain.entity.Treatment;

import java.sql.Date;
import java.util.ArrayList;

public class HistoryTest {

    private History history = new History();

    @Test
    public void testHistoryConstructor(){
        history = new History();
    }

    @Test
    public void testHistorySetters(){
        history.setId(0L);
        history.setClosed(Boolean.FALSE);
        history.setComplains(new ArrayList<Treatment>());
        history.setDiagnosis("qwerty");
        history.setDoctor(new Doctor());
        history.setDrugs(new ArrayList<Treatment>());
        history.setRegistryDate(new Date(0));
        history.setProcedures(new ArrayList<Treatment>());
    }

    @Test
    public void testHistoryGetters(){
        history.getRegistryDate();
        history.getId();
        history.getClosed();
        history.getComplains();
        history.getDiagnosis();
        history.getProcedures();
        history.getDoctor();
        history.getDrugs();
    }
}
