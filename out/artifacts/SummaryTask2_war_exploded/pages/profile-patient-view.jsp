<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="security" uri="http://security.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<security:Security hasRole="ADMIN,DOCTOR,NURSE"/>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="content.patient"/> </title>

    <!-- Bootstrap core CSS -->

    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/pages/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/pages/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/icheck/flat/green.css" rel="stylesheet">
    <!-- editor -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/editor/index.css" rel="stylesheet">
    <!-- select2 -->
    <link href="${pageContext.request.contextPath}/pages/css/select/select2.min.css" rel="stylesheet">

    <!-- switchery -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/pages/css/switchery/switchery.min.css" />

    <script src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/script.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="nav-md" onload="getAllDoctors('${pageContext.request.contextPath}')">
    <div class="container body">
        <div class="main_container">
            <%@ include file="fragments/left-bar.jspf" %>
            <%@ include file="fragments/top-navigator.jspf" %>
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2><fmt:message key="content.patient"/></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <c:if test="${sessionScope.user.role == 'ADMIN'} ">
                                        <li>
                                            <a href="#" onclick="deletePatient('${requestScope.patient.id}', '${pageContext.servletContext.contextPath}')"><i class="fa fa-close"></i></a>
                                        </li>
                                        </c:if>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab_content11" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true"><fmt:message key="content.patient"/></a>
                                            </li>
                                            <li role="presentation" class=""><a href="#tab_content22" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false"><fmt:message key="content.patient.history"/></a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent2" class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content11" aria-labelledby="home-tab">
                                                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                                                    <div class="profile_img">

                                                        <!-- end of image cropping -->
                                                        <div id="crop-avatar">
                                                            <!-- Current avatar -->
                                                            <div class="avatar-view" title="Change the avatar">
                                                                <img src="${requestScope.patient.image.imageBase64}" alt="Avatar">
                                                            </div>

                                                            <!-- Cropping modal -->
                                                            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal -->

                                                            <!-- Loading state -->
                                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                                        </div>
                                                        <!-- end of image cropping -->

                                                    </div>
                                                    <br/>
                                                    <ul class="list-unstyled user_data">
                                                        <li><i class="fa fa-map-marker user-profile-icon"></i> <fmt:message key="content.treatment.address"/>: ${requestScope.patient.address}
                                                        </li>

                                                        <li>
                                                            <i class="fa fa-briefcase user-profile-icon"></i> <fmt:message key="content.treatment.work"/>: ${requestScope.patient.work}
                                                        </li>

                                                        <li class="m-top-xs">
                                                            <i class="fa fa-external-link user-profile-icon"></i> <fmt:message key="content.treatment.telephone"/>: ${requestScope.patient.phone}
                                                        </li>
                                                    </ul>

                                                        <c:choose>
                                                            <c:when test="${requestScope.patient.discharged and sessionScope.user.role == 'ADMIN'}">
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-new-rec"><fmt:message key="content.patient.new"/></button>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:if test="${sessionScope.user.id eq requestScope.patient.medicalRecord.currentHistory.doctor.id}">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-discharge"><fmt:message key="content.patient.discharge"/></button>
                                                                </c:if>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <br />

                                                    <div class="modal fade bs-example-modal-lg-discharge" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-md">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title"><fmt:message key="content.patient.discharge"/></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="${pageContext.request.contextPath}/patient.htm" method="post">
                                                                        <fmt:message key="content.patient.discharge.info"/>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" ><fmt:message key="content.patients.additionalinformation"/> <span class="required">*</span>
                                                                            </label>
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <textarea class="form-control description" name="diagnosis" required></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" value="discharge_p" name="command"/>
                                                                        <input type="hidden" value="${param.id}" name="id"/>
                                                                        <input type="hidden" value="${requestScope.patient.medicalRecord.currentHistory.id}" name="idCurrHist"/>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                <button type="submit" class="btn btn-primary"><fmt:message key="content.patient.discharge"/></button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal fade bs-example-modal-lg-new-rec" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-md">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h4 class="modal-title" id="newRecord"><fmt:message key="content.patient.newrecord"/></h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="${pageContext.request.contextPath}/patient.htm" method="post">
                                                                        <fmt:message key="content.patient.newrecord.info"/>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-15"><fmt:message key="content.treatment.doctor"/></label>
                                                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                <select class="select2_single form-control" tabindex="-1" name="doctor" id="patDoc" style="width: 300px"></select>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" value="new_record" name="command"/>
                                                                        <input type="hidden" value="${requestScope.patient.medicalRecord.id}" name="idMedRec"/>
                                                                        <input type="hidden" value="${requestScope.patient.id}" name="id"/>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                        <button type="submit" class="btn btn-primary"><fmt:message key="content.save"/></button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- start skills -->

                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-12">

                                                    <div class="profile_title">
                                                        <div class="col-md-6">
                                                            <h2>User Activity Report</h2>
                                                        </div>
                                                    </div>
                                                    <!-- start of user-activity-graph -->
                                                    <div id="graph_bar" style="width:100%; height:280px;">
                                                                <div class="x_content">
                                                                    <br />
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><fmt:message key="content.name.first"/>:</label>${requestScope.patient.firstName}
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><fmt:message key="content.name.last"/>:</label>${requestScope.patient.secondName}
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><fmt:message key="content.name.middle"/>:</label>${requestScope.patient.middleName}
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><fmt:message key="content.treatment.gender"/>:</label>${requestScope.patient.gender}
                                                                        </div>
                                                                        <c:choose>
                                                                            <c:when test="${not requestScope.patient.discharged}">
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><fmt:message key="content.data.registry"/>:</label>${requestScope.patient.medicalRecord.currentHistory.registryDate}
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><fmt:message key="content.treatment.doctor"/>:</label>${requestScope.patient.medicalRecord.currentHistory.doctor.firstName}
                                                                                </div>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <div class="form-group">
                                                                                    <label class="control-label col-md-3"><fmt:message key="content.patient.status"/>:</label><fmt:message key="content.patient.discharged"/>
                                                                                </div>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3"><fmt:message key="content.data.birthday"/>:</label>${requestScope.patient.birthday}
                                                                        </div><%--
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">ДАТА ПОСТУПЛЕНИЯ:</label>${requestScope.patient.medicalRecord.currentHistory.registryDate}
                                                                        </div>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end of user-activity-graph -->
                                                    <c:if test="${not requestScope.patient.discharged}">
                                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                            <li role="presentation" class=""><a href="#tab_content1" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><fmt:message key="content.patient.complains"/></a>
                                                            </li>
                                                            <li role="presentation" class="active"><a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><fmt:message key="content.patient.drugs"/></a>
                                                            </li>
                                                            <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><fmt:message key="content.patient.procedures"/></a>
                                                            </li>
                                                            <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false"><fmt:message key="content.patient.operations"/></a>
                                                            </li>
                                                        </ul>
                                                        <div id="myTabContent" class="tab-content">
                                                            <div role="tabpanel" class="tab-pane fade" id="tab_content1" aria-labelledby="home-tab">
                                                                <!-- start user projects -->
                                                                <table class="data table table-striped no-margin">
                                                                    <tbody id="complains">
                                                                    <c:choose>
                                                                        <c:when test="${not empty requestScope.patient.medicalRecord.currentHistory.complains}">
                                                                            <c:forEach var="treatment" items="${requestScope.patient.medicalRecord.currentHistory.complains}">
                                                                                <tr>
                                                                                    <td>${treatment.description}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <tr>
                                                                                <td><fmt:message key="content.patient.complains.not"/></td>
                                                                            </tr>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    </tbody>
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                                <c:if test="${sessionScope.user.role == 'NURSE' or sessionScope.user.id eq requestScope.patient.medicalRecord.currentHistory.doctor.id}">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-complains"><fmt:message key="content.add"/></button>
                                                                </c:if>

                                                                <div class="modal fade bs-example-modal-lg-complains" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-md">
                                                                        <div class="modal-content">

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                                </button>
                                                                                <h4 class="modal-title" id="myModalLabelComplain"><fmt:message key="content.patient.complains"/></h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <!-- The form is placed inside the body of modal -->
                                                                                <form id="treatment-complain-form" method="post" class="form-horizontal">
                                                                                    <div class="treatment-list-drugs">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" > <fmt:message key="content.patient.complains.description"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <textarea class="form-control complain-description"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="hidden" name="idUser" value="${requestScope.patient.id}">
                                                                                </form>
                                                                                <button type="button" class="btn btn-default" onclick="addNewTreatmentComplain()"><fmt:message key="content.add"/></button>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="appointComplainTreatment('${requestScope.patient.medicalRecord.currentHistory.id}','${sessionScope.user.id}','${pageContext.request.contextPath}')"><fmt:message key="content.save"/></button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- end recent activity -->
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content2" aria-labelledby="profile-tab">
                                                                <!-- start user projects -->
                                                                <table class="data table table-striped no-margin">
                                                                    <tbody id="drugs">
                                                                    <c:choose>
                                                                        <c:when test="${not empty requestScope.patient.medicalRecord.currentHistory.drugs}">
                                                                            <c:forEach var="treatment" items="${requestScope.patient.medicalRecord.currentHistory.drugs}">
                                                                                <tr>
                                                                                    <td>${treatment.text}</td>
                                                                                    <td>${treatment.user.firstName}</td>
                                                                                    <td>${treatment.days} <fmt:message key="content.days"/></td>
                                                                                    <td>${treatment.description}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <tr>
                                                                                <td><fmt:message key="content.patient.drugs.not"/></td>
                                                                            </tr>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    </tbody>
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.drugs"/></th>
                                                                        <th><fmt:message key="content.treatment.doctor"/></th>
                                                                        <th><fmt:message key="content.days"/></th>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                                <c:if test="${sessionScope.user.role == 'NURSE' or sessionScope.user.id eq requestScope.patient.medicalRecord.currentHistory.doctor.id}">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-drugs"><fmt:message key="content.appoint"/></button>
                                                                </c:if>

                                                                <div class="modal fade bs-example-modal-lg-drugs" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-md">
                                                                        <div class="modal-content">

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                                </button>
                                                                                <h4 class="modal-title" id="myModalLabel"><fmt:message key="content.appoint"/></h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <!-- The form is placed inside the body of modal -->
                                                                                <form id="treatment-drug-form" method="post" class="form-horizontal">
                                                                                    <div class="treatment-list-drugs">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.patient.drug"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input type="text" required="required" class="form-control col-md-7 col-xs-12 treatment-drug"/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.days"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input id="date1" class="form-control col-md-7 col-xs-12 days" required="required" type="number" min="1"/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" ><fmt:message key="content.patient.complains.description"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <textarea class="form-control drug-description"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="hidden" name="idUser" value="${requestScope.patient.id}">
                                                                                </form>
                                                                                <button type="button" class="btn btn-default" onclick="addNewTreatmentDrug()"><fmt:message key="content.add"/></button>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="appointDrugTreatment('${requestScope.patient.medicalRecord.currentHistory.id}','${sessionScope.user.id}','${sessionScope.user.firstName}','${pageContext.request.contextPath}')">Save changes</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- end recent activity -->

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab2">
                                                                <table class="data table table-striped no-margin">
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.procedures"/></th>
                                                                        <th><fmt:message key="content.treatment.doctor"/></th>
                                                                        <th><fmt:message key="content.days"/></th>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="procedures">
                                                                    <c:choose>
                                                                        <c:when test="${not empty requestScope.patient.medicalRecord.currentHistory.procedures}">
                                                                            <c:forEach var="treatment" items="${requestScope.patient.medicalRecord.currentHistory.procedures}">
                                                                                <tr>
                                                                                    <td>${treatment.text}</td>
                                                                                    <td>${treatment.user.firstName}</td>
                                                                                    <td>${treatment.days} дней</td>
                                                                                    <td>${treatment.description}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <tr>
                                                                                <td><fmt:message key="content.patient.drugs.not"/></td>
                                                                            </tr>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    </tbody>
                                                                </table>
                                                                <c:if test="${sessionScope.user.role == 'NURSE' or sessionScope.user.id eq requestScope.patient.medicalRecord.currentHistory.doctor.id}">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-proced"><fmt:message key="content.appoint"/></button>
                                                                </c:if>
                                                                <div class="modal fade bs-example-modal-lg-proced" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-md">
                                                                        <div class="modal-content">

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                                </button>
                                                                                <h4 class="modal-title" id="modalProcedures"><fmt:message key="content.appoint"/></h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <!-- The form is placed inside the body of modal -->
                                                                                <form id="treatment-procedure-form" method="post" class="form-horizontal">
                                                                                    <div class="treatment-list-procedures">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.patient.procedure"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input type="text" required="required" class="form-control col-md-7 col-xs-12 treatment-procedure"/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.days"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input id="days-procedure" class="form-control col-md-7 col-xs-12 days-procedure" min="1" required="required" type="number" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" ><fmt:message key="content.patient.complains.description"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <textarea class="form-control procedure-description"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="hidden" name="idUser" value="${requestScope.patient.id}">
                                                                                </form>
                                                                                <button type="button" class="btn btn-default" onclick="addNewTreatmentProcedure('${requestScope.patient.id}')"><fmt:message key="content.appoint"/></button>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="appointProcedureTreatment('${requestScope.patient.medicalRecord.currentHistory.id}','${sessionScope.user.id}','${sessionScope.user.firstName}','${pageContext.request.contextPath}')"><fmt:message key="content.save"/></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab3">
                                                                <table class="data table table-striped no-margin">
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                        <th><fmt:message key="content.treatment.doctor"/></th>
                                                                        <th><fmt:message key="content.data.date"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody id="operations">
                                                                    <c:choose>
                                                                        <c:when test="${not empty requestScope.patient.medicalRecord.currentHistory.operations}">
                                                                            <c:forEach var="treatment" items="${requestScope.patient.medicalRecord.currentHistory.operations}">
                                                                                <tr>
                                                                                    <td>${treatment.description}</td>
                                                                                    <td>${treatment.user.firstName}</td>
                                                                                    <td>${treatment.date}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <tr>
                                                                                <td><fmt:message key="content.patient.drugs.not"/></td>
                                                                            </tr>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    </tbody>
                                                                </table>
                                                                <c:if test="${sessionScope.user.id eq requestScope.patient.medicalRecord.currentHistory.doctor.id}">
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg-operations"><fmt:message key="content.appoint"/></button>
                                                                </c:if>
                                                                <div class="modal fade bs-example-modal-lg-operations" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <div class="modal-dialog modal-md">
                                                                        <div class="modal-content">

                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                                                </button>
                                                                                <h4 class="modal-title" id="modalOperations"><fmt:message key="content.appoint"/></h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <!-- The form is placed inside the body of modal -->
                                                                                <form id="treatment-operations-form" method="post" class="form-horizontal">
                                                                                    <div class="treatment-list-operations">
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.patient.complains.description"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input type="text" required="required" class="form-control col-md-7 col-xs-12 operation-description"/>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12"><fmt:message key="content.data.date"/> <span class="required">*</span>
                                                                                            </label>
                                                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                <input id="days-operations" class="form-control col-md-7 col-xs-12 date-operation" min="1" required="required" type="date" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="hidden" name="idUser" value="${requestScope.patient.id}">
                                                                                </form>
                                                                                <button type="button" class="btn btn-default" onclick="addNewTreatmentOperation('${requestScope.patient.id}')"><fmt:message key="content.add"/></button>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="appointOperationTreatment('${requestScope.patient.medicalRecord.currentHistory.id}','${sessionScope.user.id}','${sessionScope.user.firstName}','${pageContext.request.contextPath}')"><fmt:message key="content.accept"/></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="tab_content22" aria-labelledby="profile-tab">
                                                <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">
                                                    <div class="panel">
                                                        <c:forEach var="history" items="${requestScope.patient.medicalRecord.histories}">
                                                        <a class="panel-heading" role="tab" id="headingOne${history.id}" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne${history.id}" aria-expanded="false" aria-controls="collapseOne">
                                                            <h4 class="panel-title">${history.registryDate}</h4>
                                                        </a>
                                                        <div id="collapseOne${history.id}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body"><%--
                                                                <p><strong>Collapsible Item 2 data</strong></p>--%>
                                                                <fmt:message key="content.patient.complains"/>
                                                                <table class="data table table-striped no-margin">
                                                                    <tbody>
                                                                            <c:forEach var="complain" items="${history.complains}">
                                                                                <tr>
                                                                                    <td>${complain.description}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                    </tbody>
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                                <br/>
                                                                <fmt:message key="content.treatment"/>:
                                                                <table class="data table table-striped no-margin">
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.drug"/></th>
                                                                        <th><fmt:message key="content.treatment.doctor"/></th>
                                                                        <th><fmt:message key="content.days"/></th>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <c:forEach var="treatment" items="${history.drugs}">
                                                                            <tr>
                                                                                <td>${treatment.text}</td>
                                                                                <td>${treatment.user.firstName}</td>
                                                                                <td>${treatment.days}</td>
                                                                                <td>${treatment.description}</td>
                                                                            </tr>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                                <fmt:message key="content.patient.procedures"/>:
                                                                <table class="data table table-striped no-margin">
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.procedure"/></th>
                                                                        <th><fmt:message key="content.treatment.doctor"/></th>
                                                                        <th><fmt:message key="content.days"/></th>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <c:forEach var="treatment" items="${history.procedures}">
                                                                        <tr>
                                                                            <td>${treatment.text}</td>
                                                                            <td>${treatment.user.firstName}</td>
                                                                            <td>${treatment.days} <fmt:message key="content.days"/></td>
                                                                            <td>${treatment.description}</td>
                                                                        </tr>
                                                                    </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                                <fmt:message key="content.patient.operations"/>:
                                                                <table class="data table table-striped no-margin">
                                                                    <thead>
                                                                    <tr>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                        <th><fmt:message key="content.patient.complains.description"/></th>
                                                                        <th><fmt:message key="content.data.date"/></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                            <c:forEach var="treatment" items="${history.operations}">
                                                                                <tr>
                                                                                    <td>${treatment.description}</td>
                                                                                    <td>${treatment.user.firstName}</td>
                                                                                    <td>${treatment.date}</td>
                                                                                </tr>
                                                                            </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                                <fmt:message key="content.treatment.diagnose"/>: ${history.diagnosis}
                                                            </div>
                                                        </div>
                                                            <br/>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- footer content -->
                <%@ include file="fragments/footer.jspf" %>
                <!-- /footer content -->

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="${pageContext.request.contextPath}/pages/js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="${pageContext.request.contextPath}/pages/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="${pageContext.request.contextPath}/pages/js/icheck/icheck.min.js"></script>
    <!-- tags -->
    <script src="${pageContext.request.contextPath}/pages/js/tags/jquery.tagsinput.min.js"></script>
    <!-- switchery -->
    <script src="${pageContext.request.contextPath}/pages/js/switchery/switchery.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/moment.min2.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/datepicker/daterangepicker.js"></script>
    <!-- richtext editor -->
    <script src="${pageContext.request.contextPath}/pages/js/editor/bootstrap-wysiwyg.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/editor/external/jquery.hotkeys.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/editor/external/google-code-prettify/prettify.js"></script>
    <!-- select2 -->
    <script src="${pageContext.request.contextPath}/pages/js/select/select2.full.js"></script>
    <!-- form validation -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/parsley/parsley.min.js"></script>
    <!-- textarea resize -->
    <script src="${pageContext.request.contextPath}/pages/js/textarea/autosize.min.js"></script>
    <script>
        autosize($('.resizable_textarea'));
    </script>
    <!-- Autocomplete -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/autocomplete/countries.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/autocomplete/jquery.autocomplete.js"></script>
    <script type="text/javascript">
        $(function () {
            'use strict';
            var countriesArray = $.map(countries, function (value, key) {
                return {
                    value: value,
                    data: key
                };
            });
            // Initialize autocomplete with custom appendTo:
            $('#autocomplete-custom-append').autocomplete({
                lookup: countriesArray,
                appendTo: '#autocomplete-container'
            });
        });
    </script>
    <script src="${pageContext.request.contextPath}/pages/js/custom.js"></script>


    <!-- select2 -->
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "Select a state",
                allowClear: true
            });
            $(".select2_group").select2({});
            $(".select2_multiple").select2({
                maximumSelectionLength: 4,
                placeholder: "With Max Selection limit 4",
                allowClear: true
            });
        });
    </script>
    <!-- /select2 -->
    <!-- input tags -->
    <script>
        function onAddTag(tag) {
            alert("Added a tag: " + tag);
        }

        function onRemoveTag(tag) {
            alert("Removed a tag: " + tag);
        }

        function onChangeTag(input, tag) {
            alert("Changed a tag: " + tag);
        }

        $(function () {
            $('#tags_1').tagsInput({
                width: 'auto'
            });
        });
    </script>
    <!-- /input tags -->
    <!-- form validation -->
    <script type="text/javascript">
        $(document).ready(function () {
            $.listen('parsley:field:validate', function () {
                validateFront();
            });
            $('#demo-form .btn').on('click', function () {
                $('#demo-form').parsley().validate();
                validateFront();
            });
            var validateFront = function () {
                if (true === $('#demo-form').parsley().isValid()) {
                    $('.bs-callout-info').removeClass('hidden');
                    $('.bs-callout-warning').addClass('hidden');
                } else {
                    $('.bs-callout-info').addClass('hidden');
                    $('.bs-callout-warning').removeClass('hidden');
                }
            };
        });

        $(document).ready(function () {
            $.listen('parsley:field:validate', function () {
                validateFront();
            });
            $('#demo-form2 .btn').on('click', function () {
                $('#demo-form2').parsley().validate();
                validateFront();
            });
            var validateFront = function () {
                if (true === $('#demo-form2').parsley().isValid()) {
                    $('.bs-callout-info').removeClass('hidden');
                    $('.bs-callout-warning').addClass('hidden');
                } else {
                    $('.bs-callout-info').addClass('hidden');
                    $('.bs-callout-warning').removeClass('hidden');
                }
            };
        });
        try {
            hljs.initHighlightingOnLoad();
        } catch (err) {}
    </script>
    <!-- /form validation -->
    <!-- editor -->
    <script>
        $(document).ready(function () {
            $('.xcxc').click(function () {
                $('#descr').val($('#editor').html());
            });
        });

        $(function () {
            function initToolbarBootstrapBindings() {
                var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                            'Times New Roman', 'Verdana'],
                        fontTarget = $('[title=Font]').siblings('.dropdown-menu');
                $.each(fonts, function (idx, fontName) {
                    fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
                });
                $('a[title]').tooltip({
                    container: 'body'
                });
                $('.dropdown-menu input').click(function () {
                    return false;
                })
                        .change(function () {
                            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                        })
                        .keydown('esc', function () {
                            this.value = '';
                            $(this).change();
                        });

                $('[data-role=magic-overlay]').each(function () {
                    var overlay = $(this),
                            target = $(overlay.data('target'));
                    overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                });
                if ("onwebkitspeechchange" in document.createElement("input")) {
                    var editorOffset = $('#editor').offset();
                    $('#voiceBtn').css('position', 'absolute').offset({
                        top: editorOffset.top,
                        left: editorOffset.left + $('#editor').innerWidth() - 35
                    });
                } else {
                    $('#voiceBtn').hide();
                }
            };

            function showErrorAlert(reason, detail) {
                var msg = '';
                if (reason === 'unsupported-file-type') {
                    msg = "Unsupported format " + detail;
                } else {
                    console.log("error uploading file", reason, detail);
                }
                $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
            };
            initToolbarBootstrapBindings();
            $('#editor').wysiwyg({
                fileUploadError: showErrorAlert
            });
            window.prettyPrint && prettyPrint();
        });
    </script>
    <!-- /editor -->

</body>

</html>