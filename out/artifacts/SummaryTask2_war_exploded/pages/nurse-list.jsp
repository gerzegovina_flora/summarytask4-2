<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://security.pivovarenko.nure.ua" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:Security hasRole="ADMIN,DOCTOR,NURSE"/>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="content.nurses"/> </title>
    <!-- Bootstrap core CSS -->

    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/pages/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="${pageContext.request.contextPath}/pages/css/custom.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/icheck/flat/green.css" rel="stylesheet">


    <script src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>

    <script src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="${pageContext.request.contextPath}/pages/js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="${pageContext.request.contextPath}/pages/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="${pageContext.request.contextPath}/pages/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="${pageContext.request.contextPath}/pages/js/icheck/icheck.min.js"></script>

    <script src="${pageContext.request.contextPath}/pages/js/custom.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <script src="${pageContext.request.contextPath}/pages/js/script.js"></script>

</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">

            <%@ include file="fragments/left-bar.jspf" %>
            <%@ include file="fragments/top-navigator.jspf" %>
            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3><fmt:message key="content.nurses"/>
                                <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                <a href="${pageContext.request.contextPath}/pages/add-nurse.jsp"><span class="glyphicon glyphicon-pencil"></span></a>
                            </c:if>
                            </h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">
                                    <div class="row">
                                        <div class="clearfix"></div>

                                        <c:forEach var="nurse" items="${requestScope.nurseList}">
                                        <div class="col-md-4 col-sm-4 col-xs-12 animated fadeInDown" id="nurse${nurse.id}">
                                            <div class="well profile_view">
                                                <div class="col-sm-12">
                                                    <h4 class="brief"><i><fmt:message key="content.nurses"/></i></h4>
                                                    <div class="left col-xs-7">
                                                        <h2>${nurse.firstName} ${nurse.secondName}</h2><br/><br/>
                                                    </div>
                                                    <div class="right col-xs-5 text-center">
                                                        <img src="${nurse.image.imageBase64}" alt="" class="img-circle img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 bottom text-center">
                                                    <div class="col-xs-12 col-sm-8 emphasis">
                                                        <c:if test="${sessionScope.user.role == 'ADMIN'}">
                                                            <a href="${pageContext.request.contextPath}/nurse-list.htm?command=info_nurse&id=${nurse.id}"><button type="button" class="btn btn-warning btn-xs"> <i class="fa fa-user">
                                                            </i> <fmt:message key="content.edit"/> </button></a>
                                                            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bs-example-modal-sm${nurse.id}"> <i class="fa fa-user">
                                                            </i> <fmt:message key="content.delete"/> </button>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="modal fade bs-example-modal-sm${nurse.id}" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-sm">
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel2"><fmt:message key="content.delete.confirm"/></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4><fmt:message key="content.delete"/></h4>
                                                            <p><fmt:message key="content.delete.confirm.nurse"/></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message key="content.cancel"/></button>
                                                                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteNurse('${pageContext.request.contextPath}',${nurse.id})"><fmt:message key="content.delete.confirm.nurse"/></button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                    <%@ include file="fragments/footer.jspf" %>
                </div>
            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

</body>

</html>